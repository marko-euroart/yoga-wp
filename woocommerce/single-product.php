<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$chained_products_id = $post->ID;
$total_chained_details  = $wc_chained_products->get_all_chained_product_details( $chained_products_id );

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php 
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header-image' );
	$style = '';
	if( !empty($thumb) ) {
		$url = $thumb['0']; 
		$style = 'style="background: url('. $url .') no-repeat center bottom; background-size:cover"';
	}
?>

<div id="promo" class="privacy product-header-image" <?php echo $style; ?>>
	<div class="container">
		
		<h2 class="title-3">
			<?php 
				$product = wc_get_product( get_the_ID() );
				if( $product->is_type( 'simple' ) && empty($total_chained_details) ){
				  echo '<span class="uppercase">Video</span>';
				} else {
				  echo '<span class="uppercase">Paket</span>';
				}
			?>
			<?php the_title(); ?>
			<?php 
			$instructor_id = get_field('select_instructor'); 
			if( $instructor_id ) {
				echo '<span>'. get_the_title($instructor_id) . '</span>';
			}
			?>
		</h2>

	</div>
</div>


<?php
$current_user = wp_get_current_user();
$date = date('Y-m-d H:i:s');
$now = time();
$downloads_table = $wpdb->prefix . 'woocommerce_downloadable_product_permissions';
$userResults = $wpdb->get_results( "SELECT * FROM $downloads_table WHERE `user_id` = $current_user->ID AND `product_id` = $product->id AND `user_email` = '$current_user->user_email' AND `access_expires` >= '$date'");
array_reverse($userResults);
if(!empty($userResults) && empty($total_chained_details)) :
	$expires = $userResults[0]->access_expires;
	if ( wc_customer_bought_product( $current_user->user_email, $current_user->ID, $product->id) && $expires >= $date ): ?>

	<div id="main">
		<section id="video-screen">
			<div class="container">
			<?php if( $product->is_type( 'simple' ) && empty($total_chained_details) ): ?>
				<?php 
				$vimeo_sample = '';
				$downloads = WC()->customer->get_downloadable_products();
				if( !empty($downloads) ) {
					foreach ( $downloads as $download ) {
						if( $download['product_id'] == $post->ID ) {
							$vimeo_full_url = $download['file']['file'];
							break;
						}
					}
					$vimeo_full = (int) substr(parse_url($vimeo_full_url, PHP_URL_PATH), 1);
				}
				?>
				<div class="video-wrapper">
					<iframe src="https://player.vimeo.com/video/<?php echo $vimeo_full; ?>" width="100%" height="auto" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
			<?php endif; ?>

				<div class="main-content">
					
					<h3 class="title-5 lock"><?php the_title(); ?></h3>

					<h2 class="title-6">
						<?php 
							$terms = get_the_terms( $product->id, 'product_cat' );
							$cat_name = '';
							if( !empty($terms) ) {
								foreach ($terms as $term) {
								    $cat_name = '<span class="category">'. $term->name .'</span>';
								    break;
								}
							}
						?>
						<?php 
						$current_user = wp_get_current_user();
						$date = date('Y-m-d H:i:s');
						$now = time();
						$downloads_table = $wpdb->prefix . 'woocommerce_downloadable_product_permissions';
						$userResults = $wpdb->get_results( "SELECT * FROM $downloads_table WHERE `user_id` = $current_user->ID AND `product_id` = $product->id AND `user_email` = '$current_user->user_email' AND `access_expires` >= '$date'");
						if(!empty($userResults)) {
							$expires = $userResults[0]->access_expires;
							if ( wc_customer_bought_product( $current_user->user_email, $current_user->ID, $product->id) && $expires >= $date ) {
								$remaining_days = floor((strtotime($expires)-$now)/(60*60*24));
								$last_day_num = substr($remaining_days, -1);
								$day_string = $last_day_num == 1 ? 'dan' : 'dana';
								$product_status = 'Do isteka: '.$remaining_days.' '.$day_string.'';
								$payment_status = 'purchased';
							}
						}
						?>
						<div class="video-bought-share">
							<?php echo do_shortcode( '[ssba]' ); ?>
						</div>
						<span class="time"><?php echo $product_status; ?></span>
					</h2>

					<div class="user-content">
						<?php the_content(); ?>
						<?php get_template_part( 'partials/notice-box' ); ?>
					</div>
					
					
					<?php /*
					<p class="alert lock"><a href="#">Prijavite se</a> ili <a href="#">Registrirajte se</a> kako bi kupili
					pojedinačni video te ostvarili <a href="#">Dodatne povoljnosti</a></p>
					*/ ?>

				</div>

				
				<div class="side-content">

					<?php if( $instructor_id ) : ?>
					<div class="video-description">
						<div class="avatar">
							<p>Instruktor</p>
							<?php if( has_post_thumbnail($instructor_id) ) : ?>
							<div class="thumb-avatar">
								<?php echo get_the_post_thumbnail( $instructor_id, 'instructor-thumb-small' );  ?>
							</div>
							<?php endif; ?>
						</div>
						<?php 
							$ins_name = get_the_title($instructor_id); 
							$pieces = explode(' ', $ins_name);
							echo '<p class="name">'. $pieces[0] .' <span>'. $pieces[1] .'</span></p>';
						?>
						<p><?php echo get_field('intro_text', $instructor_id); ?></p>

						<?php 
						$cert_image = get_field('cert_image', $instructor_id);
						if($cert_image): ?>
						<div class="thumb cert-image">
							<img src="<?php echo $cert_image['sizes']['cert-image'] ?>" alt="<?php echo $cert_image['alt']; ?>">
						</div>
						<?php endif; ?>
						<a href="<?php echo get_permalink( wpml_id(9) ); ?>?section=inst<?php echo $instructor_id; ?>">Pogledajte profil</a>
				
					</div>
					<?php endif; ?>

				</div>

			</div>
		</section>
	</div>

<?php	
	endif;
	else:
?>

<div id="main">
	<section id="chosen-package">
		<div class="container">
			<div class="main-content">
			<?php if( $product->is_type( 'simple' ) && empty($total_chained_details) ): ?>
				<?php
					$vimeo_sample_url = get_field('sample_video_url', $product->id ); 
					$vimeo_sample = (int) substr(parse_url($vimeo_sample_url, PHP_URL_PATH), 1);
				?>
				<div class="video-wrapper">
					<iframe src="https://player.vimeo.com/video/<?php echo $vimeo_sample; ?>" width="100%" height="auto" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
				<?php echo do_shortcode( '[ssba]' ); ?>
				<?php /*
				<div class="video-share">
					<a href="" class="share-link facebook">Podijelite ovaj video</a>
				</div>
				*/ ?>
			<?php endif; ?>
				<article class="user-content">
					<h2 class="title-6">
						<?php 
							$terms = get_the_terms( $product->id, 'product_cat' );
							$cat_name = '';
							if(!empty($terms)) {
								foreach ($terms as $term) {
								    $cat_name = '<span class="category">'. $term->name .'</span>';
								    break;
								}
							}
						?>
						<?php the_title(); ?>

						<?php if( !empty($total_chained_details) ): ?>
							<span class="contains"><?php echo count($total_chained_details); ?> videa</span>
						<?php endif; ?>

						<?php 
							$video_duration = get_field('video_duration', $product->id ); 
							if( $video_duration ) {
								echo '<p class="time">'.  $video_duration . '"</p>';
							}
						?>
						
					</h2>
					<?php the_content(); ?>

					<?php get_template_part( 'partials/notice-box' ); ?>

				</article>
			</div>
			<div class="side-content">
				<div class="price-box">
					<?php
					$product_price = $product->get_price_html(); 

					if( $product->is_type( 'simple' ) && empty($total_chained_details) ){
						  echo '<p>'. __('Cijena videa', 'yoga') . $product_price .'(30 dana)</p>';
						} else {
						  echo '<p>'. __('Cijena paketa', 'yoga') . $product_price .'(30 dana)</p>';
						}
					?>
					
					<?php

					if(!empty($userResults)) {
						$expires = $userResults[0]->access_expires;
						if ( wc_customer_bought_product( $current_user->user_email, $current_user->ID, $product->id) && $expires >= $date ) {
							$remaining_days = floor((strtotime($expires)-$now)/(60*60*24));
							$last_day_num = substr($remaining_days, -1);
							$day_string = $last_day_num == 1 ? 'dan' : 'dana';
							echo '<a class="btn-3">Video kupljen</a>';
							echo '<a class="added_to_cart">Vrijedi: <strong><u>'.$remaining_days.' '.$day_string.'</u></strong></a>';
						}
					}
					elseif(sizeof( $woocommerce->cart->cart_contents) > 0) {
						$in_cart = 'false';
						foreach($woocommerce->cart->get_cart() as $cart_item_key => $values ) {
							$_product = $values['data'];
							if( get_the_ID() == $_product->id ) {
								$in_cart = 'true';
							}
						}
						if( $in_cart == 'false' ) {
							echo apply_filters( 'woocommerce_loop_add_to_cart_link',
								sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="btn-3 color-2 %s"><i class="icon-cart"></i>%s</a>',
									esc_url( $product->add_to_cart_url() ),
									esc_attr( isset( $quantity ) ? $quantity : 1 ),
									esc_attr( $product->id ),
									esc_attr( $product->get_sku() ),
									esc_attr( isset( $class ) ? $class : 'button' ),
									esc_html( $product->add_to_cart_text() )
								),
							$product );
						}
					}
					elseif ( $product->is_purchasable() && $product->is_in_stock() ) {
						echo apply_filters( 'woocommerce_loop_add_to_cart_link',
								sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="btn-3 color-2 %s"><i class="icon-cart"></i>%s</a>',
									esc_url( $product->add_to_cart_url() ),
									esc_attr( isset( $quantity ) ? $quantity : 1 ),
									esc_attr( $product->id ),
									esc_attr( $product->get_sku() ),
									esc_attr( isset( $class ) ? $class : 'button' ),
									esc_html( $product->add_to_cart_text() )
								),
							$product );
					}
					
					if ( sizeof( $woocommerce->cart->cart_contents) > 0 && empty($userResults) ) :
						echo '<a class="added_to_cart wc-forward" href="' . $woocommerce->cart->get_checkout_url() . '" title="' . __( 'Pogledaj košaricu' ) . '">' . __( 'Pogledaj košaricu' ) . '</a>';
					endif;
					?>
					
				</div>
				<?php if( $instructor_id ) : ?>
				<div class="video-description">
					<div class="avatar">
						<p>Instruktor</p>
						<?php if( has_post_thumbnail($instructor_id) ) : ?>
						<div class="thumb-avatar">
							<?php echo get_the_post_thumbnail( $instructor_id, 'instructor-thumb-small' );  ?>
						</div>
						<?php endif; ?>
					</div>
					<?php 
						$ins_name = get_the_title($instructor_id); 
						$pieces = explode(' ', $ins_name);
						echo '<p class="name">'. $pieces[0] .' <span>'. $pieces[1] .'</span></p>';
					?>
					<p><?php echo get_field('intro_text', $instructor_id); ?></p>

					<?php 
					$cert_image = get_field('cert_image', $instructor_id);
					if($cert_image): ?>
					<div class="thumb cert-image">
						<img src="<?php echo $cert_image['sizes']['cert-image'] ?>" alt="<?php echo $cert_image['alt']; ?>">
					</div>
					<?php endif; ?>
					<a href="<?php echo get_permalink( wpml_id(9) ); ?>?section=inst<?php echo $instructor_id; ?>">Pogledajte profil</a>
					
				</div>
				

				<?php endif; ?>
			</div>
			<?php if( !empty($total_chained_details) ): ?>
			<div class="video-blocks">
				<h3 class="title-4">Video sadržaj iz paketa</h3>
				<div class="video-blocks-wrapper">
					<?php echo do_shortcode( '[chained_products]' );; ?>
				</div>
			</div>
			<?php endif; ?>


		</div>
	</section>	

	<?php get_template_part( 'partials/popular-videos' ); ?>

</div>
<?php
endif;
endwhile; 
// end of the loop. 

?>
<?php get_footer(); ?>