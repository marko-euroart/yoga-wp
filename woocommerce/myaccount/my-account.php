<h1 class="title-1">
	Pregled
	<span>kupljenog sadržaja</span>
</h1>

<?php
    $user_id = get_current_user_id();
    $current_user= wp_get_current_user();
    $customer_email = $current_user->email;
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1
        );
    $loop = new WP_Query( $args );
    if ( $loop->have_posts() ): 
?>
<div class="video-blocks">
	<div class="video-blocks-wrapper" data-method="eqHeight">
		<?php
	   //      while ( $loop->have_posts() ) : $loop->the_post(); 
	        	
	   //      	global $wc_chained_products;
				// $chained_products_id = $loop->post->ID;
				// $total_chained_details  = $wc_chained_products->get_all_chained_product_details( $chained_products_id );
				// if( empty($total_chained_details) ) continue; 
					
	   //      	$_product = get_product( $loop->post->ID );
		  //       if (wc_customer_bought_product($customer_email, $user_id, $_product->id)) {
		  //           woocommerce_get_template_part( 'content', 'product' );
		  //       }

	   //      endwhile;
		?>
		<?php
	        while ( $loop->have_posts() ) : $loop->the_post(); 
	        	
	        	global $wc_chained_products;
				$chained_products_id = $loop->post->ID;
				$total_chained_details  = $wc_chained_products->get_all_chained_product_details( $chained_products_id );
				if( !empty($total_chained_details) ) continue; 
					
	        	$_product = get_product( $loop->post->ID );
		        if (wc_customer_bought_product($customer_email, $user_id, $_product->id)) {
		            woocommerce_get_template_part( 'content', 'product' );
		        }

	        endwhile;
		?>
	</div>
</div>
<?php else: ?>
<div class="video-blocks">
	<h3 class="title-4">Nemate zakupljen niti jedan video</h3>
</div>
<?php endif;  wp_reset_postdata(); ?>


<?php 
// function check_is_category_A_customer(array $product_ids)
// {
// $product_ids= array ('2258','2253','2242');//for testing
// global $woocommerce;
// $user_id = get_current_user_id();
// $current_user= wp_get_current_user();
// $customer_email = $current_user->email;


// foreach($product_ids as $item):
//     if ( wc_customer_bought_product( $customer_email, $user_id, $item) )
//        return true;
//     endforeach; 

// return false;
// }

/*
<div class="video-blocks">
	<div class="video-blocks-wrapper" data-method="eqHeight">
		<div class="block">
			<div class="video-preview">
				<a href="#">
					<img src="<?php echo site_url(); ?>img/photo/sample-video-4.jpg" alt="Sample video">
				</a>
			</div>
			<div class="video-info purchased">
				<p><span>Početnici</span> Smooth &amp; Free</p>
				<p class="name">Ružica Matagić</p>
				<p class="time">25:30"</p>
				<p class="countdown">Do isteka: 20 dana, 18 sati</p>
			</div>
		</div>
		<div class="block">
			<div class="video-preview">
				<a href="#">
					<img src="<?php echo site_url(); ?>img/photo/sample-video-5.jpg" alt="Sample video">
				</a>
			</div>
			<div class="video-info purchased">
				<p><span>Napredni</span> Smooth &amp; Free</p>
				<p class="name">Ružica Matagić</p>
				<p class="time">25:30"</p>
				<a href="" class="renew">Obnova najma</a>
			</div>
		</div>
		<div class="block">
			<div class="video-preview">
				<a href="#">
					<img src="<?php echo site_url(); ?>img/photo/sample-video-6.jpg" alt="Sample video">
				</a>
			</div>
			<div class="video-info purchased">
				<p><span>Učitelji</span> Smooth &amp; Free</p>
				<p class="name">Ružica Matagić</p>
				<p class="time">25:30"</p>
				<p class="countdown">Do isteka: 20 dana, 18 sati</p>
			</div>
		</div>
		<div class="block">
			<div class="video-preview">
				<a href="#">
					<img src="<?php echo site_url(); ?>img/photo/sample-video-4.jpg" alt="Sample video">
				</a>
			</div>
			<div class="video-info purchased">
				<p><span>Početnici</span> Smooth &amp; Free</p>
				<p class="name">Ružica Matagić</p>
				<p class="time">25:30"</p>
				<p class="countdown">Do isteka: 20 dana, 18 sati</p>
			</div>
		</div>
		<div class="block">
			<div class="video-preview">
				<a href="#">
					<img src="<?php echo site_url(); ?>img/photo/sample-video-5.jpg" alt="Sample video">
				</a>
			</div>
			<div class="video-info purchased">
				<p><span>Napredni</span> Smooth &amp; Free</p>
				<p class="name">Ružica Matagić</p>
				<p class="time">25:30"</p>
				<a href="" class="renew">Obnova najma</a>
			</div>
		</div>
		<div class="block">
			<div class="video-preview">
				<a href="#">
					<img src="<?php echo site_url(); ?>img/photo/sample-video-6.jpg" alt="Sample video">
				</a>
			</div>
			<div class="video-info purchased">
				<p><span>Učitelji</span> Smooth &amp; Free</p>
				<p class="name">Ružica Matagić</p>
				<p class="time">25:30"</p>
				<p class="countdown">Do isteka: 20 dana, 18 sati</p>
			</div>
		</div>
		<div class="block">
			<div class="video-preview">
				<a href="#">
					<img src="<?php echo site_url(); ?>img/photo/sample-video-4.jpg" alt="Sample video">
				</a>
			</div>
			<div class="video-info purchased">
				<p><span>Početnici</span> Smooth &amp; Free</p>
				<p class="name">Ružica Matagić</p>
				<p class="time">25:30"</p>
				<p class="countdown">Do isteka: 20 dana, 18 sati</p>
			</div>
		</div>
		<div class="block">
			<div class="video-preview">
				<a href="#">
					<img src="<?php echo site_url(); ?>img/photo/sample-video-5.jpg" alt="Sample video">
				</a>
			</div>
			<div class="video-info purchased">
				<p><span>Napredni</span> Smooth &amp; Free</p>
				<p class="name">Ružica Matagić</p>
				<p class="time">25:30"</p>
				<a href="" class="renew">Obnova najma</a>
			</div>
		</div>
		<div class="block">
			<div class="video-preview">
				<a href="#">
					<img src="<?php echo site_url(); ?>img/photo/sample-video-6.jpg" alt="Sample video">
				</a>
			</div>
			<div class="video-info purchased">
				<p><span>Učitelji</span> Smooth &amp; Free</p>
				<p class="name">Ružica Matagić</p>
				<p class="time">25:30"</p>
				<p class="countdown">Do isteka: 20 dana, 18 sati</p>
			</div>
		</div>
	</div>
	
</div>
*/ ?>