<?php
/**
 * Login Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>


        <h2><?php _e( 'Register', 'woocommerce' ); ?></h2>

        <form method="post" class="register login-form">

            <?php do_action( 'woocommerce_register_form_start' ); ?>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
            <div class="group">
                <input type="text" placeholder="<?php _e( 'Username', 'woocommerce' ); ?>" class="input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
            </div>
            <?php endif; ?>

            <div class="group">
                <input type="email" placeholder="<?php _e( 'Email address', 'woocommerce' ); ?>" class="input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
            </div>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
            <div class="group">
                <input type="password" placeholder="<?php _e( 'Password', 'woocommerce' ); ?>" class="input-text" name="password" id="reg_password" />
            </div>
            <?php endif; ?>

            <!-- Spam Trap -->
            <div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

            <?php do_action( 'woocommerce_register_form' ); ?>
            <?php do_action( 'register_form' ); ?>

            <div class="group">
                <p class="form-row">
                    <?php wp_nonce_field( 'woocommerce-register' ); ?>
                    <button type="submit" class="button btn-1" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_attr_e( 'Register', 'woocommerce' ); ?></button>
                </p>
            </div>

            <?php do_action( 'woocommerce_register_form_end' ); ?>

        </form>


<?php endif; ?>

		<form method="post" class="login-form">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<div class="group">
				<input type="text" class="input-text" name="username" id="username" placeholder="E mail ili Korisničko ime" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
            </div>
            <div class="group">
            	<input class="input-text" type="password" name="password" id="password" placeholder="Lozinka" />
            </div>
            
            <?php do_action( 'woocommerce_login_form' ); ?>

            <div class="group checkbox">
                <input type="checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" data-method="checkBox">
                <label for="rememberme">Zapamti me</label>
            </div>
            <div class="group">
            <?php wp_nonce_field( 'woocommerce-login' ); ?>
                <button type="submit" class="btn-1" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_attr_e( 'Login', 'woocommerce' ); ?></button>
            </div>
            <div class="group">
                <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="lost-password"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
            </div>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
