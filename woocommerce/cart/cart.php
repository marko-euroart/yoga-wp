<?php
/**
 * Cart Page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();
do_action( 'woocommerce_before_cart' ); ?>

<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
		
<?php do_action( 'woocommerce_before_cart_table' ); ?>
 
		<div class="order-box shop_table shop_table_responsive cart">

			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			$chained_products = array();
			$regular_products = array();
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				
				if( !empty($cart_item['chained_item_of']) ) {
					$chained_products[] = $cart_item['product_id'];
				}
				else {
					$regular_products[] = $cart_item['product_id'];
				}
			} 
			foreach ( $regular_products as $regular_product ) {

		        foreach ($chained_products as $chained_product) {
		        	if( $regular_product == $chained_product ) {
						WC()->cart->set_quantity( $regular_product, $cart_item['quantity'] - 1, true  );
			        }
		        }

		    }

			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
			?>

			<div class="order <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
				<div class="half">
					<p>
					<?php
					if ( ! $_product->is_visible() ) {
							echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
						} else {
							echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
						}
					?>
					<?php 
						$instructor_id = get_field('select_instructor', $cart_item['product_id'] ); 
						if( $instructor_id ) {
							echo '<span>'. get_the_title($instructor_id) . '</span>';
						}
					?>
					</p>
				</div>
				<div class="half price">
					<p>
					<?php
						echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
					?>
					</p>
					<!-- <p>80,00 <span>HRK</span></p> -->
				</div>
				<?php
					echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
						'<a href="%s" class="close-order" title="%s" data-product_id="%s" data-product_sku="%s"><span>&times;</span></a>',
						esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
						__( 'Remove this item', 'woocommerce' ),
						esc_attr( $product_id ),
						esc_attr( $_product->get_sku() )
					), $cart_item_key );
				?>
				<!-- <a href="#" class="close-order"><span>Close order</span></a> -->
			</div>
			<?php
					}
				}

			do_action( 'woocommerce_cart_contents' );
			?>

		</div>
		<?php /*
		<?php if ( wc_coupons_enabled() ) { ?>
			<div class="coupon">

				<label for="coupon_code"><?php _e( 'Coupon', 'woocommerce' ); ?>:</label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />

				<?php do_action( 'woocommerce_cart_coupon' ); ?>
			</div>
		<?php } ?>

		<input type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" />
		*/ ?>
		<?php do_action( 'woocommerce_cart_actions' ); ?>
		<?php wp_nonce_field( 'woocommerce-cart' ); ?>


	<?php do_action( 'woocommerce_after_cart_table' ); ?>

</form>

<div id="order_review" class="woocommerce-checkout-review-order">
	<div class="total-price">
		<p><?php _e( 'Total', 'woocommerce' ); ?> <span><?php wc_cart_totals_order_total_html(); ?></span></p>
	</div>
</div>

<?php 
	global $woocommerce;
	echo '<a class="added_to_cart wc-forward" href="' . $woocommerce->cart->get_checkout_url() . '" title="' . __( 'Plaćanje' ) . '">' . __( 'Plaćanje' ) . '</a>';
?>

<?php do_action( 'woocommerce_after_cart' ); ?>
