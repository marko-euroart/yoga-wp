<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

$c = $woocommerce_loop['loop'];
$c = $c % 8;
if( $c == 0 ) {
	$c = 8;
}

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ) {
	$classes[] = 'odd';
}
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}
if ( $woocommerce_loop['loop'] % 3 == 0 || $woocommerce_loop['loop'] == 1 ) {
	$classes[] = 'odd';
}
// var_dump($product);
?>

<div class="block">
	<div class="video-preview">
		<a href="<?php echo $product->get_permalink(); ?>">
			<?php echo $product->get_image( 'product-thumb' ); ?>
		</a>
	</div>

	<?php
		$current_user = wp_get_current_user();
		$date = date('Y-m-d H:i:s');
		$now = time();
		$downloads_table = $wpdb->prefix . 'woocommerce_downloadable_product_permissions';
		$userResults = $wpdb->get_results( "SELECT * FROM $downloads_table WHERE `user_id` = $current_user->ID AND `product_id` = $product->id AND `user_email` = '$current_user->user_email' AND `access_expires` >= '$date'");
		if(!empty($userResults)) {
			$expires = $userResults[0]->access_expires;
			if ( wc_customer_bought_product( $current_user->user_email, $current_user->ID, $product->id) && $expires >= $date ) {
				$remaining_days = floor((strtotime($expires)-$now)/(60*60*24));
				$last_day_num = substr($remaining_days, -1);
				$day_string = $last_day_num == 1 ? 'dan' : 'dana';
				$product_status = '<p class="countdown">Do isteka: '.$remaining_days.' '.$day_string.'</p>';
				$payment_status = 'purchased';
			}
		}
		elseif ( wc_customer_bought_product( $current_user->user_email, $current_user->ID, $product->id) && $expires < $date ) {
			$payment_status = 'purchased';
			$product_status = '<a href="'.$product->get_permalink().'" class="renew">Obnova najma</a>';
		}
		else {
			$payment_status = '';
			$product_status = '';
		}
	?>

	<div class="video-info <?php echo $payment_status; ?>">
		<?php 
			$terms = get_the_terms( $product->id, 'product_cat' );
			$cat_name = '';
			if( !empty($terms) ) {
				foreach ($terms as $term) {
				    $cat_name = '<span>'. $term->name .'</span>';
				    break;
				}
			}
		?>
		<p><?php echo $cat_name; ?> <?php echo $product->get_title(); ?></p>
		<?php
			$instructor_id = get_field('select_instructor', $product->id ); 
			if( $instructor_id ) {
				echo '<p class="name">'. get_the_title($instructor_id) . '</p>';
			}

			$video_duration = get_field('video_duration', $product->id ); 
			if( $video_duration ) {
				echo '<p class="time">'.  $video_duration . '"</p>';
			}
			echo $product_status; 
		?>
	</div>
</div>