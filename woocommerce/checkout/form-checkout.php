<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>

<form name="checkout" method="post" id="payment-form" class="checkout woocommerce-checkout user-content" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">

	<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

			<?php do_action( 'woocommerce_checkout_billing' ); ?>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

		<h2 id="order_review_heading"><em><?php _e( 'Your order', 'woocommerce' ); ?></em></h2>

	<?php endif; ?>


	<?php do_action( 'woocommerce_before_cart_table' ); ?>

		<div class="order-box shop_table shop_table_responsive cart">

			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
			?>

			<div class="order <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
				<div class="half">
					<p>
					<?php
					if ( ! $_product->is_visible() ) {
							echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
						} else {
							echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
						}
					?>
					<?php 
						$instructor_id = get_field('select_instructor', $cart_item['product_id'] ); 
						if( $instructor_id ) {
							echo '<span>'. get_the_title($instructor_id) . '</span>';
						}
					?>
					</p>
				</div>
				<div class="half price">
					<p>
					<?php
						echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
					?>
					</p>
					<!-- <p>80,00 <span>HRK</span></p> -->
				</div>
				<?php
					echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
						'<a href="%s" class="close-order" title="%s" data-product_id="%s" data-product_sku="%s"><span>&times;</span></a>',
						esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
						__( 'Remove this item', 'woocommerce' ),
						esc_attr( $product_id ),
						esc_attr( $_product->get_sku() )
					), $cart_item_key );
				?>
				<!-- <a href="#" class="close-order"><span>Close order</span></a> -->
			</div>
			<?php
					}
				}

			do_action( 'woocommerce_cart_contents' );
			?>

		</div>


	<?php // do_action( 'woocommerce_after_cart_table' ); ?>

	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>


	<p><em>Nastavkom pristajete na naše <a href="<?php echo get_permalink( wpml_id(13) ); ?>"><span>Uvjete korištenja</span></a></em></p>

	<div class="certificate-thumb">
		<img src="<?php echo get_template_directory_uri(); ?>/img/assets/certificate.jpg" alt="Security certificate">
	</div>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>


