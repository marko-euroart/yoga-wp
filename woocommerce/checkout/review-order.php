<?php
/**
 * Review order table
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="total-price">
	<p><?php _e( 'Total', 'woocommerce' ); ?> <span><?php wc_cart_totals_order_total_html(); ?></span></p>
</div>