<?php get_header(); ?>

<div id="promo" class="video-shop">
	<div class="container">
		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h2 class="title-3">
				<?php woocommerce_page_title(); ?>
				<span>sve što vam treba za prakticiranje yoge...</span>
			</h2>
		<?php endif; ?>

	</div>
</div>

<div id="main">

	<section id="video-offers">
	<?php /*
		<form action="#" class="filter-videos" data-method="fancySelect">
			<div class="container">
				<div class="group">
					<select>
						<option value="default-level" selected disabled>Težina</option>
						<option value="level-1">Težina 1</option>
						<option value="level-2">Težina 2</option>
						<option value="level-3">Težina 3</option>
					</select>
				</div>
				<!-- <div class="group">
					<select>
						<option value="default-time" selected disabled>Vrijeme</option>
						<option value="time-1">Vrijeme 1</option>
						<option value="time-2">Vrijeme 2</option>
						<option value="time-3">vrijeme 3</option>
					</select>
				</div> -->
				<div class="group">
					<select>
						<option value="default-style" selected disabled>Stilovi</option>
						<option value="style-1">Stil 1</option>
						<option value="style-2">Stil 2</option>
						<option value="style-3">stil 3</option>
					</select>
				</div>
				<div class="group">
					<select>
						<option value="default-instructors" selected disabled>Instruktori</option>
						<option value="instructors-1">Instruktor 1</option>
						<option value="instructors-2">Instruktor 2</option>
						<option value="instructors-3">Instruktor 3</option>
					</select>
				</div>
				<div class="group search">
					<input type="text">
					<button type="submit"></button>
				</div>
			</div>
		</form>
	*/ ?>




		<div class="container">
			<div class="number-of-results">
			<?php
				$args = array( 'post_type' => 'product', 'post_status' => 'publish', 'posts_per_page' => -1 );
				$products = new WP_Query( $args );
				echo '<p><span>'. $products->found_posts .' rezultata</p>';
				wp_reset_query();
			?>
			</div>


		<?php 
			$args = array(
			    'posts_per_page' => -1,
			    'tax_query' => array(
			        //'relation' => 'AND',
			        array(
				       'taxonomy' => 'product_type',
				      'field' => 'slug',
				      'terms' => 'simple'
			        )
			    ),
			    'post_type' => 'product',
			    'orderby' => 'title',
			);
			$the_query = new WP_Query( $args );
		?>

		<?php if ( $the_query->have_posts() ) : ?>

			<?php woocommerce_product_loop_start(); ?>

			<?php woocommerce_product_subcategories(); ?>

			<div class="video-blocks">
				<h3 class="title-4">Paketi</h3>
				<div class="video-blocks-wrapper">

					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php 
						$chained_products_id = $post->ID;
						$total_chained_details  = $wc_chained_products->get_all_chained_product_details( $chained_products_id );
						if( empty($total_chained_details) ) continue; 
						?>
						<?php wc_get_template_part( 'content', 'product' ); ?>

					<?php endwhile; // end of the loop. ?>

					
				</div>
			</div>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif; wp_reset_query(); ?>


		<?php 
			$args = array(
			    'posts_per_page' => -1,
			    'tax_query' => array(
			        //'relation' => 'AND',
			        array(
				       'taxonomy' => 'product_type',
				      'field' => 'slug',
				      'terms' => 'simple'
			        )
			    ),
			    'post_type' => 'product',
			    'orderby' => 'title',
			);
			$the_query = new WP_Query( $args );
		?>

		<?php if ( $the_query->have_posts() ) : ?>

			<?php woocommerce_product_loop_start(); ?>

			<?php woocommerce_product_subcategories(); ?>

			<div class="video-blocks">
				<h3 class="title-4">Pojedinačni</h3>
				<div class="video-blocks-wrapper">

					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php 
						$chained_products_id = $post->ID;
						$total_chained_details  = $wc_chained_products->get_all_chained_product_details( $chained_products_id );
						if( !empty($total_chained_details) ) continue; 
						?>
						<?php wc_get_template_part( 'content', 'product' ); ?>

					<?php endwhile; // end of the loop. ?>

					
				</div>
			</div>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif; wp_reset_query(); ?>

		
	
			<!-- <div class="pagination">
				<a href="#" class="prev">Prev</a>
				<ul>
					<li class="active"><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
				</ul>
				<a href="#" class="next">Next</a>
			</div> -->
		</div>
	</section>


	<?php get_template_part( 'partials/popular-videos' ); ?>

</div>

<?php get_footer(); ?>