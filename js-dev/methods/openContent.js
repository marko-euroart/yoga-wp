EA.Methods.openContent = function($this){

	var article = $this.closest('.instructor-wrapper').find('.instructor-content');

	$this.click(function(e){
		e.preventDefault();

		if (article.hasClass('active')) {
			article.slideUp(1000).removeClass('active');
		} else {
			$('.instructor-content').slideUp(1000).removeClass('active');
			article.slideDown(1000).addClass('active');
		}

	});

	$('.close-content').click(function(e){
		e.preventDefault();
		article.slideUp(1000).removeClass('active');
	});

}
