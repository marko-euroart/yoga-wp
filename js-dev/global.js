$(document).ready(function(){

	function getURLParameter(name) {
      return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
    }

    var myvar = getURLParameter('section');

    var instructor_id = $(".instructor-profile");
    instructor_id.each(function() {
        data_id = $(this).attr("data-id");
        if ( myvar == 'inst'+data_id ) {
          $('html, body').animate({
                scrollTop: $("#inst"+data_id).offset().top
            }, 1000);
        }

    });

    $('.openLogin').click(function(e){
        e.preventDefault();
        $('.body-wrapper').addClass('login-active');
        $('.login').addClass('active');
    });

    equalheight = function(container){
        var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
        $(container).each(function() {
            $el = $(this);
            $($el).height('auto')
            topPostion = $el.position().top;
            if (currentRowStart != topPostion) {
                for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs.length = 0; // empty the array
                currentRowStart = topPostion;
                currentTallest = $el.height();
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
            }
            
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
        });
    }

    equalheight('.video-blocks-wrapper .block');


    $(".fancybox").fancybox({
        padding: 0,
        
    });

    $(".fancybox-media").fancybox({
        padding: 0,
        helpers : {
            media : {}
        }
    });

});

$(window).load(function() {
  equalheight('.video-blocks-wrapper .block');
});
$(window).resize(function(){
  equalheight('.video-blocks-wrapper .block');
});