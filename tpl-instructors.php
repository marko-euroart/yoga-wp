<?php 
//Template name: Instructors
get_header(); ?>

<?php 
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header-image' );
	$style = '';
	if( !empty($thumb) ) {
		$url = $thumb['0']; 
		$style = 'style="background: url('. $url .') no-repeat center bottom"';
	}
?>

<div id="promo" class="home-instructors" <?php echo $style; ?>>
	<div class="container">
		<h1 class="title-3">
			<?php the_title(); ?>
			<?php if( get_field('subtitle') ) {
				echo '<span>'. get_field('subtitle') .'</span>';
			} ?>
		</h1>
	</div>
</div>


<?php $q = new WP_Query( array( 'posts_per_page' => -1, 'post_type' => 'instructor', 'order' => 'ASC', 'orderby' => 'menu_order' ) ); ?>
<?php if( $q->have_posts() ) : ?>
<div id="main">
	<section id="instruktori">

		<?php $c=1; $d=1; while( $q->have_posts() ) : $q->the_post(); ?>
		<?php 
			$cert_name = get_field('cert_name');
			$cert_image = get_field('cert_image');
			$classes = get_field('classes');
			$intro_text = get_field('intro_text');
			if( $d % 6 == 0 ) {
				$d = 1;
			}
			$instructor_background_image = get_field('instructor_background_image');
			if( $instructor_background_image ) {
				$inst_bg = 'style="background: url('. $instructor_background_image['url'] .') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;"';
			}
			if( empty($classes) ) {
				$noclasses = 'no-class';
			} else {
				$noclasses = '';
			}
		?>
		<div class="instructor-wrapper instructor-profile" id="inst<?php echo get_the_id(); ?>" data-id="<?php echo get_the_id(); ?>">
			<div class="instructor-preview instructor-<?php echo $d; ?>" <?php echo $inst_bg; ?>>
				<div class="container">
					<div class="info-wrapper">
						<?php if( has_post_thumbnail() ) : ?>
						<div class="thumb">
							<?php the_post_thumbnail( 'instructor-thumb'); ?>
						</div>
						<?php endif; ?>
						<div class="info">
							<?php 
								$ins_name = get_the_title(); 
								$pieces = explode(' ', $ins_name);
								echo '<p>'. $pieces[0] .' <span>'. $pieces[1] .'</span></p>';
							?>
							<?php if($cert_image): ?>
							<img src="<?php echo $cert_image['sizes']['cert-image'] ?>" alt="<?php echo $cert_image['alt']; ?>">
							<?php endif; ?>
						</div>
					</div>
					<?php if( $intro_text ): ?>
					<div class="quote">	
						<blockquote><span>“</span><?php echo $intro_text; ?></blockquote>
						<a href="#" class="open-content" data-method="openContent">open</a>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="instructor-content active">
				<div class="container">
					<?php if( $classes ): ?>
					<div class="class-info">
						<h3><?php _e('Satovi', 'yoga'); ?></h3>
						<ul>
						<?php 
							$classes_array = explode("\n", $classes);
							foreach ($classes_array as $class) {
								echo '<li>'. $class .'</li>';
							}
						?>
						</ul>
					</div>
					<?php endif; ?>
					<article class="user-content <?php echo $noclasses; ?>">
						<?php the_content(); ?>
						<a href="#" class="close-content">close</a>
					</article>
				</div>
			</div>
		</div>
		<?php $c++; $d++; endwhile; ?>


	</section>
</div>
<?php endif; wp_reset_postdata(); ?>

<?php get_footer(); ?>