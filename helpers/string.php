<?php
/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 * classes.php sadržava pomoćne CSS klase npr: first, even, odd, 
 * Bitno je da svi counteri pocinju sa brojem 1, a ne nula;
 **/

function word_limiter($str, $limit = 30, $end_char = "")
	{
		if (trim($str) == '')
		{
			return $str;
		}

		preg_match('/^\s*+(?:\S++\s*+){1,'.(int) $limit.'}/', $str, $matches);

		if (strlen($str) == strlen($matches[0]))
		{
			$end_char = '';
		}

		return rtrim($matches[0]).$end_char;
	}

// ako je selektirano 
function class_selected($va1, $val2){
	if( $val1 == $val2 ){
		echo 'class="selected"';
	}
}

// dodaje samo selected (bez  class="")
function item_selected($va1, $val2){
	if( $val1 == $val2 ){
		echo "selected";
	}
}

// sa kounterom sve
function class_first($c, $str = 'first'){
	echo $c == 1 ? ' class="'.$str.'"' : "";
}

// Vraca samo first, nije vezano za klasu
function item_first($c) {
	echo $c == 1 ? ' first' : "";
}

function even_odd($c){
	if( $c % 2 == 0 ) {
		echo ' even';
	} else {
		echo ' odd';
	}
}


function cycle()
{
	$args= func_get_args();
	$c = array_shift($args) - 1;
	echo $args[($c % count($args))];
}


//alternate( $c, array( 4 => 'first', 2 => 'first draco' ) );
function alternate($c, $args )
{
	foreach( $args as $k => $v ){
		if( $c % $k == 1 ){
			echo $v;
		}
	}
}


function alter_class($c, $args) {
	if( !isset( $args ) || !is_array( $args ) ) 
		trigger_error("Second argumnet must be an array.", E_USER_ERROR);;

	foreach( $args as $k => $v ){
		if( $c % $k == 0 ){
			$class_names[] = $v;
		}
	}

	if( !empty( $class_names ) ) {
		echo 'class="'.implode(" ", $class_names).'"';
	}

}

function contains_substr($substring, $string) {
	$pos = strpos($string, $substring);

	if($pos === false) {
	        // string needle NOT found in haystack
	        return false;
	}
	else {
	        // string needle found in haystack
	        return true;
	}
}