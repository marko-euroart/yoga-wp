<?php
/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 * Klasa za vrijeme...koristi google weather API i sadrzi neke isprravke oko dijakritickih znakova za hr jezik - nije aktivno vise
 **/
class Weather {

	public $url; // pogledati __construct
	public $temp; // Dodaje  "ºC" ili "ºF" ovisno i jeziku.

	//Odmah inicijaliziramo url kroz klasu
	function __construct( $lang="hr", $city='Zagreb,Croatia' ) {
		$url = 'http://www.google.com/ig/api?&weather='.$city.'&hl='.$lang;
		$this->url = $url;
		$this->temp = $lang == 'hr' ? "ºC" : "ºF";
	}

	//-----------------------------------------------------------------------

	// Pošto hr jezik ima dikariticke znakove xml se lomi 
	// !!! Ovo jos pogledati kako popraviti
	protected function sanitize($str){
	 	$chr  = array( "è", '¾', 'æ', "¹" );
	 	$nchr = array( "č", "ž", 'ć', "š" );

	 	return str_replace($chr, $nchr, $str );
	}

	//-----------------------------------------------------------------------

	// Dohvacamo xml podatke za nas grad i na jeziku
	protected function get_xml(){
		$content = utf8_encode( file_get_contents($this->url) );
		return simplexml_load_string( $content );
	}

	//-----------------------------------------------------------------------

	// Funckija dohvaca smo podatke za ovaj trenutak
	public function current($data="") {

		$xml = $this->get_xml();
		$current = $xml->xpath("/xml_api_reply/weather/current_conditions"); 

		$condition 	= $current[0]->condition['data'];
		$temp_c    	= $current[0]->temp_c['data'];
		$temp_f    	= $current[0]->temp_f['data'];
		$humidity  	= $current[0]->humidity['data'];
		$image     	= basename($current[0]->icon['data']);
		$new_image 	= 'http://ssl.gstatic.com/onebox/weather/60/'.str_replace('gif', 'png', $image);    
		$wind     	=  $current[0]->wind_condition['data'];


		$result = array(
			'condition' 	=> $this->sanitize($condition),
			'temp_c' 		=> $temp_c.'ºC', 
			'temp_f' 		=> $temp_f.'ºF',
			'humidity' 		=> $this->sanitize($humidity), // 
			'image'			=> $new_image,
			'wind' 			=> str_replace(" S ", " Sjever, ", $wind )
		);

		return $result[$data];
	}

	//-----------------------------------------------------------------------

	// Dohvaca podatk za prognozu za dananji dan +3 dana unaprijed
	// Podaci koji se vracaju: day_of_week, low, high, icon, condition;
	public function forecast(){
		$xml = $this->get_xml();
		$forecast = $xml->xpath("/xml_api_reply/weather/forecast_conditions");

		return $forecast;
	}

	//-----------------------------------------------------------------------

	// Funkcija koje dohvacaju rezultate za forecast u foreach() loop-u
	public function day_of_week($data)  { return $this->sanitize($data->day_of_week['data']); }
	public function low($data)			{ return $data->low['data'] . $this->temp; }
	public function high($data)			{ return $data->high['data'] . $this->temp; }
	public function icon($data)			{ return $data->icon['data']; }
	public function condition($data)	{ return $this->sanitize($data->condition['data']); }

}