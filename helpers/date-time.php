<?php
/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 * date-time sadržava vecinu pomocnih funkcija vezanih za datume i vrijeme
 **/

// @since v1.0 
// Prevodimo datume na hrv bez obzira na to jeli instalcija na hr ili en
function eng_to_cro_date($str)
{
	$eng = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	$hr  = array('siječnja', 'veljače', 'ožujka', 'travnja', 'svibnja', 'lipnja', 'srpnja', 'kolovoza', 'rujna', 'listopada', 'studenog', 'prosinca' );
	$result_hr = apply_filters('cro_date_names', $hr);
	return str_replace($eng, $result_hr, $str);
}


// @since v1.0 
// Prevodimo dane na hrv bez obzira na to jeli instalcija na hr ili en
function eng_to_cro_day( $str ){
	$eng = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' );
	$hr  = array('ponedjeljak', 'utorak', 'srijeda', 'četvrtak', 'petak', 'subota', 'nedjelja');

	$result_hr = apply_filters('cro_time_names', $hr );
	return str_replace($eng, $result_hr, $str);
}

// @since v1.0 
// Prikazujemo datum ali na svakom postu, koristi se u loop-u ili na single,page...
function wp_date($echo=true, $format=''){
	global $post;

	if( $format == '' ) {
		$format = get_option('date_format');
	}

	$date = get_the_time( $format );

	if( ICL_LANGUAGE_CODE == 'hr' ) {
		$date = eng_to_cro_date( eng_to_cro_day( $date ) );
	} 

	if($echo==false){
		return $date;
	} else {
		echo $date;
	}
}



function return_date( $format = null ){
	global $post;

	$f = $format == null ? get_option('date_format') : $format;
	$date = get_the_time($f);
	return $date;
}

// @since v1.0 
// Prikazujemo datum ali na svakom postu i to na hrv jeziku bez obzira na to je li wp instalacija na hr ili en, koristi se u loop-u ili na single,page...
function cro_date($echo=true){
	global $post;
	$date = eng_to_cro_day(eng_to_cro_date( get_the_time(get_option('date_format')) ) );
	if($echo=false){
		return $date;
	} else {
		echo $date;
	}
}

// @since v1.0 
// Prikazujemo trenutni datum prema formatu zadanom u adminu, uz funkciju eng_to_cro_date imena datuma se moze prevesti 
function get_current_date($echo=false) {
	$now = gmdate('Y-m-d H:i:00'); 
	$date = mysql2date('Y-m-d H:i:00', $now);
	if($echo==false){
		return $date;
	} else {
		echo $date;
	}
}

function ea_get_date( $string ) {
	$date = mysql2date( get_option('date_format'), $string );
	if( ICL_LANGUAGE_CODE == 'hr' ) {
		return eng_to_cro_date( eng_to_cro_day( $date ) );
	} else {
		return $date;	
	}
	
}

function ea_get_time( $string, $format="" ) {
	if( $format == "" ) {
		$date = mysql2date( 'H:i', $string );
	} else {
		$date = mysql2date( $format, $string );
	}

	if( ICL_LANGUAGE_CODE == 'hr' ) {
		return eng_to_cro_date( eng_to_cro_day( $date ) );
	} else {
		return $date;	
	}
}

// @since v1.0 
// Prikazujemo samo ime danauz funkciju eng_to_cro_day imena datuma se moze prevesti na hr
function get_current_day($echo=false) {
	$day  = date("l");
	return $day;
}

// @since v1.0 
//	Relativno vrijeme
function get_relative_time() {
 
	global $post;
 
	$date = get_post_time('G', true, $post);

	$chunks = array(
		array( 60 * 60 * 24 * 365 , __( 'year' ), __( 'years' ) ),
		array( 60 * 60 * 24 * 30 , __( 'month' ), __( 'months' ) ),
		array( 60 * 60 * 24 * 7, __( 'week' ), __( 'weeks' ) ),
		array( 60 * 60 * 24 , __( 'day' ), __( 'days' ) ),
		array( 60 * 60 , __( 'hour' ), __( 'hours' ) ),
		array( 60 , __( 'minute' ), __( 'minutes' ) ),
		array( 1, __( 'second' ), __( 'seconds' ) )
	);
 
	if ( !is_numeric( $date ) ) {
		$time_chunks = explode( ':', str_replace( ' ', ':', $date ) );
		$date_chunks = explode( '-', str_replace( ' ', '-', $date ) );
		$date = gmmktime( (int)$time_chunks[1], (int)$time_chunks[2], (int)$time_chunks[3], (int)$date_chunks[1], (int)$date_chunks[2], (int)$date_chunks[0] );
	}
 
	$current_time = current_time( 'mysql', $gmt = 0 );
	$newer_date = strtotime( current_time('mysql') );
	//$newer_date = current_time('timestamp');

	$since = $newer_date - $date;
 
	if ( 0 > $since )
		return __( 'sometime' );
 
	for ( $i = 0, $j = count($chunks); $i < $j; $i++) {
		$seconds = $chunks[$i][0];
 
		if ( ( $count = floor($since / $seconds) ) != 0 )
			break;
	}
 
	$output = ( 1 == $count ) ? '1 '. $chunks[$i][1] : $count . ' ' . $chunks[$i][2];
 
	if ( !(int)trim($output) ){
		$output = '0 ' . __( 'seconds' );
	}
 
	$output .= __(' ago');
 
	return $output;
}


// @since v1.0 
//	Relativno vrijeme na hr
function get_relative_time_cro($piece="") {
  global $post;
  $time = get_the_time('Y/m/d H:i:s');
  //$time = current_time('mysql');

  $min   = abs(ceil((strtotime($time)-strtotime(current_time('mysql')))/60));
  $hours = abs(ceil((strtotime($time)-strtotime(current_time('mysql')))/3600));
  $days = abs(ceil((strtotime($time)-strtotime(current_time('mysql')))/86400));
  $weeks = abs(ceil((strtotime($time)-strtotime(current_time('mysql')))/604800));
  $months  = abs(ceil((strtotime($time)-strtotime(current_time('mysql')))/2419200));
  $years  = abs(ceil((strtotime($time)-strtotime(current_time('mysql')))/29030400));

  $timepast = "init";


  if( $min < 60 && $hours == 0 )
      { 
      	$minutu = array(1, 21, 31, 41, 51 );
      	$minute = array(2,3,4,22,23,24,32,33,34,42,43,44,52,53,54);

      	if( in_array($min, $minutu) ) {
      		$timepast = $min ." ". __('minutu');
      	} elseif( in_array($min, $minute) ) {
      		$timepast = $min ." ". __('minute');
      	} else {
      		$timepast = $min ." ". __('minuta');
      	}
         // echo $min . '  min';
       }
  elseif( $hours < 24 && $days == 0 )
      { 
        // gledam koji je sat i po tome odlucujem

        $sati = array(5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

        if( $hours == 1 || $hours == 21 ) {
          $timepast = $hours ." ". __('sat');
        } elseif( in_array($hours, $sati) ) { 
          $timepast = $hours ." ". __('sati');
        } else {
          $timepast = $hours ." ". __('sata');
        }
        // echo $hours . ' h'; 

      }
  elseif ($days < 7 && $weeks == 0 ) 
     { 
        $timepast = $days == 1 ? $days ." ". __('dan') : $days ." " . __('dana');
       //echo $days . ' days'; 
     }
  elseif( $weeks < 4 && $months == 0 )
     { 
       $timepast = $weeks == 1 ? " ". __('tjedan dana') : $weeks ." ". __('tjedna');
     }
  elseif( $months < 12 && $years == 0 ) 
     { 
        if( $months == 1 ) {
          $timepast = " ".__("mjesec dana");
        } elseif( $months > 1 && $months < 5 ) {
          $timepast = $months ." ".__('mjeseca');
        } else {
           $timepast = $months ." ". __('mjeseci');
        }
     }
  elseif( $years > 0 ) 
     {  
       $timepast = $years == 1 ? " ". __("godinu dana") : $years ." ". __('godine');
     }  
  else 
     { $timepast = __('nepoznato'); }

	if( $piece != "" ) :
		$result = explode(" ", $timepast );
		
		if( $piece == 'time' ) {
			return $result[0];
		} elseif( $piece == 'suffix' ) {
			return $result[1];
		} else {
			return "";
		}

	else :
		$result = $timepast;
		return $result;
	endif;
}

function wp_archive_dropdown(){
	global $wpdb, $wp_locale, $wp_query;
	$month_names = array('Siječanj', 'Veljača', 'Ožujak', 'Travanj', 'Svibanj', 'Lipanj', 'Srpanj', 'Kolovoz', 'Rujan', 'Listopad', 'Studeni', 'Prosinac' );
	$join = apply_filters( 'getarchives_join', '', $r );
	$where = apply_filters( 'getarchives_where', "WHERE post_type = 'post' AND post_status = 'publish'", $r );
	$query = "SELECT YEAR(post_date) AS year, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date) ORDER BY post_date DESC";
    $key = md5($query);
    $cache = wp_cache_get( 'wp_get_archives_' , 'general');
    if ( !isset( $cache[ $key ] ) ) {
            $arcresults = $wpdb->get_results($query);
            $cache[ $key ] = $arcresults;
            wp_cache_set( 'wp_get_archives_', $cache, 'general' );
    } else {
            $arcresults = $cache[ $key ];
    }

     $c=0; foreach( $arcresults as $arc ) {
        $arc_year = $arc->year;
     	$query = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS month, count(ID) as posts FROM $wpdb->posts WHERE post_type = 'post' AND post_status = 'publish' AND YEAR(post_date) = $arc_year GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date ASC ";
     	// echo $query;
     	$key = md5($query);	                
     	$cache_ = wp_cache_get( 'wp_get_archives_m' , 'general');
		if ( !isset( $cache_[ $key ] ) ) {
		        $arcresults_m = $wpdb->get_results($query);
		        $cache_[ $key ] = $arcresults_m;
		        wp_cache_set( 'wp_get_archives_m', $cache_, 'general' );
		} else {
		        $arcresults_m = $cache[ $key ];
		}

		$months = array();
		if( !empty( $arcresults_m ) ) {
			foreach( $arcresults_m as $result ) {
				$months[] = $result->month;
			}
		}
		$arcresults[$c]->months = $months;
		$c++;
     }

    $archive_data = $wp_query->query_vars;
    foreach( $arcresults as $result ) :
    	$current_year = $archive_data['year'] == $result->year ? ' class="active"' : '';
    ?>
	<ul>
		<li<?php echo $current_year;  ?>><a href="#"><?php echo $result->year; ?></a>
			<?php if( isset( $result->months ) && !empty( $result->months ) ) : ?>
				<ul>
				<?php foreach ($result->months as $month ): ?>
					<?php $current_month = ( $archive_data['monthnum'] == $month && $current_year != '' ) ? ' class="active"' : ''; ?>
					<li<?php echo $current_month;  ?>><a href="<?php echo get_month_link( $result->year, $month ); ?>"><?php echo $month_names[ $month ]; ?></a></li>		
				<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		<li>
	</ul>

	<?php
	endforeach;
}
// end of date-time file
// end of date-time.php file