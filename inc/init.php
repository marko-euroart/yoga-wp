<?php 

// Funkcija za pozivanje helpera
function load_helper($helpers){
	if( !is_array( $helpers ) ) {
		require( get_template_directory() . "/helpers/$helpers.php" );
	}
	if( is_array( $helpers ) ) {
		foreach( $helpers as $helper ) {
			require( get_template_directory() . "/helpers/$helper.php" );
		}
	}
}

function load_model($modules) {
	if( !is_array( $modules ) ) {
		require( get_template_directory() . "/modules/$modules.php" );
	}
	if( is_array( $modules ) ) {
		foreach( $modules as $modul ) {
			require( get_template_directory() . "/modules/$modul.php" );
		}
	}
}

function load($files){
	if( !is_array( $files ) ) {
		require( get_template_directory() . "/inc/$files.php" );
	}
	if( is_array( $files ) ) {
		foreach( $files as $file ) {
			require( get_template_directory() . "/inc/$file.php" );
		}
	}
}

function load_class( $files ) {
	if( !is_array( $files ) ) {
		require( get_template_directory() . "/inc/$files/$files.php" );
	}
	if( is_array( $files ) ) {
		foreach( $files as $file ) {
			require( get_template_directory() . "/inc/$file/$file.php" );
		}
	}
}


// Funkcija za pozivanje login stilova za EA93
function add_ea93_login()
{
	require_once( get_template_directory() . '/inc/ea-login/login.php' ); 
}

// Funkcija za pozivanje ea93 admin stilova i funkcionalnosti
function add_ea93_admin()
{
	require_once( get_template_directory() . '/inc/ea-admin/admin.php' ); 	
}