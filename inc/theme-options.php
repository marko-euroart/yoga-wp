<?php
/**
 *
 * @package WordPress 
 * @subpackage wp_starter
 * Definirane su sve dodatne opcije za temu
 *
**/
function display_option($option,$txt="") {
    if( get_option($option) != "" ) {
        echo get_option($option);
    } else {
        echo $txt;
    }
}
function display_option_ml($option,$txt="") {
    if( get_option($option . '_'.ICL_LANGUAGE_CODE ) != "" ) {
        echo get_option($option .'_'.ICL_LANGUAGE_CODE );
    } else {
        echo $txt;
    }
}
function return_option($option,$txt="") {
    if( get_option($option) != "" ) {
        return get_option($option);
    } else {
        return $txt;
    }
}
function return_option_ml($option,$txt="") {
    if( get_option( $option .'_'.ICL_LANGUAGE_CODE ) != "" ) {
        return get_option( $option .'_'.ICL_LANGUAGE_CODE );
    } else {
        return $txt;
    }
}

function get_option_val_tag($option, $tag_open="", $tag_close="" ) {
    if( get_option($option) != "" ) {
        return $tag_open .  get_option($option) . $tag_close ;
    } 
}

function get_postmeta_val_tag($option, $tag_open="", $tag_close="" ) {
    if( get_postmeta_val($option) != "" ) {
        return $tag_open .  get_postmeta_val($option) . $tag_close ;
    } 
}

// adding styles 
function enqueue_theme_styles() {
    wp_enqueue_style('thickbox');
  //  wp_enqueue_style( 'header-theme-options', get_template_directory_uri() . '/inc/theme-options.css', false, '2012-02-10' );
}

//adding iis_get_script_map(server_instance, virtual_path, script_extension)
function enqueue_theme_scripts() {
    wp_enqueue_script('tiny_mce');
    wp_enqueue_script('media-upload');
    //wp_enqueue_script('thickbox');
   // wp_enqueue_script('header-theme-script', get_template_directory_uri() . '/inc/theme-options.js', false, '2012-02-10');
}

// acctualy hook our scripts and style if we on theme page
if( ( isset($_GET['page']) && $_GET['page'] == 'theme-options') ) {
    add_action('admin_print_styles', 'enqueue_theme_styles');
    add_action('admin_print_scripts', 'enqueue_theme_scripts');
}

add_action('admin_menu', 'contact_option_menu');
//addign submenu
function contact_option_menu() {
    add_submenu_page('themes.php', 'Site Info', 'Site Info', 'manage_options', 'theme-options', 'callback_theme_options');
    add_action('admin_init', 'register_theme_options');
}

function register_theme_options() {
    register_setting( 'theme-options-group', 'facebook' );
    register_setting( 'theme-options-group', 'twitter' );
    register_setting( 'theme-options-group', 'linked_in' );
    register_setting( 'theme-options-group', 'youtube' );
    register_setting( 'theme-options-group', 'main_email' );

    register_setting( 'theme-options-group', 'location' );
    register_setting( 'theme-options-group', 'number_mb' );
    register_setting( 'theme-options-group', 'main_tel' );
    register_setting( 'theme-options-group', 'number_oib' );
    register_setting( 'theme-options-group', 'number_bank' );
    register_setting( 'theme-options-group', 'main_tel' );
    register_setting( 'theme-options-group', 'main_fax' );
    
    register_setting( 'theme-options-group', 'the_price_date_' . ICL_LANGUAGE_CODE );


    register_setting( 'theme-options-group', 'tagline_1_'.ICL_LANGUAGE_CODE );
    register_setting( 'theme-options-group', 'tagline_2_'.ICL_LANGUAGE_CODE );
    register_setting( 'theme-options-group', 'email_'.ICL_LANGUAGE_CODE );
    register_setting( 'theme-options-group', 'tel_'.ICL_LANGUAGE_CODE );
    register_setting( 'theme-options-group', 'fax_'.ICL_LANGUAGE_CODE );
    register_setting( 'theme-options-group', 'location_'.ICL_LANGUAGE_CODE );
    register_setting( 'theme-options-group', 'company_address_'.ICL_LANGUAGE_CODE );


}

function callback_theme_options() {
?>
<div class="wrap" style="padding:20px">
<?php screen_icon(); ?>
<h2><?php _e('Site information'); ?></h2>
<?php settings_errors(); ?>
<br><br>
<form method="post" action="options.php">
    <?php settings_fields( 'theme-options-group' ); ?>

    <div class="theme-box">
        <?php /*
        <p><label><?php _e('Front Page Video'); ?></label><br> 
            <script type="text/javascript">
                jQuery(document).ready(function(){
    
                console.log("script for uploading uploaded");
                jQuery('#upload_img').click(function(){
                    formfield = jQuery("#front_video").attr("name");
                    tb_show('', 'media-upload.php?TB_iframe=1');
                    console.log("tb shows");
                    return false;
                });

                window.send_to_editor = function(html) {
                    console.log(html);
                    var imgurl = jQuery(html).attr('href');


                    console.log(imgurl);

                    jQuery('#front_video').val(imgurl);
                    tb_remove();
                }


});
            </script>
            <input type="text" style="width:300px;" id="front_video" class="up_video" name="front_video" value="<?php echo get_option('front_video'); ?>" /> <a href="#" id="upload_img" class="button">Upload image</a><br>
        </p> */ ?>
        <div class="company_networks">
            <h2>Contact info</h2>
            <p>
            <label for="<?php echo 'location' ?>">Location</label><br>
            <textarea name="<?php echo 'location' ?>" id="<?php echo 'location' ?>" cols="30" rows="10" style="width: 300px;"><?php echo get_option( 'location' ); ?></textarea>
            </p>
            <p>
            <label for="<?php echo 'main_email' ?>">E-mail</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'main_email' ?>" id="<?php echo 'main_email' ?>" value="<?php echo get_option( 'main_email' ); ?>">
            </p>
            <p>
            <label for="<?php echo 'number_mb' ?>">MB</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'number_mb' ?>" id="<?php echo 'number_mb' ?>" value="<?php echo get_option( 'number_mb' ); ?>">
            </p>
            <p>
            <label for="<?php echo 'number_oib' ?>">OIB</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'number_oib' ?>" id="<?php echo 'number_oib' ?>" value="<?php echo get_option( 'number_oib' ); ?>">
            </p>
            <p>
            <label for="<?php echo 'number_bank' ?>">Račun</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'number_bank' ?>" id="<?php echo 'number_bank' ?>" value="<?php echo get_option( 'number_bank' ); ?>">
            </p>
            <p>
            <label for="<?php echo 'main_tel' ?>">Tel.</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'main_tel' ?>" id="<?php echo 'main_tel' ?>" value="<?php echo get_option( 'main_tel' ); ?>">
            </p>
            <p>
            <label for="<?php echo 'main_fax' ?>">Fax.</label><br>
            <input style="width:500px;padding:7px" type="text" name="<?php echo 'main_fax' ?>" id="<?php echo 'main_fax' ?>" value="<?php echo get_option( 'main_fax' ); ?>">
            </p>
        </div>    

<?php /*          <div class="taglines">
            <h2>Site Taglines</h2>
            <p>
                <label for="<?php echo 'tagline_1_'. ICL_LANGUAGE_CODE; ?>">Site tagline</label><br>
                <input style="width:500px;padding:7px" type="text" name="<?php echo 'tagline_1_'.ICL_LANGUAGE_CODE ?>" id="<?php echo 'tagline_1_'.ICL_LANGUAGE_CODE ?>" value="<?php echo get_option('tagline_1_'.ICL_LANGUAGE_CODE); ?>">
            </p>
            <p>
                <label for="<?php echo 'tagline_2_'.ICL_LANGUAGE_CODE ?>">Second tagline</label><br>
                <input style="width:500px;padding:7px" type="text" name="<?php echo 'tagline_2_'.ICL_LANGUAGE_CODE ?>" id="<?php echo 'tagline_2_'.ICL_LANGUAGE_CODE ?>" value="<?php echo get_option('tagline_2_'.ICL_LANGUAGE_CODE); ?>">
            </p> 
        </div>  */ ?>
        
        <div class="company_networks">
            <h2>Social Networks</h2>
        	<p>
        		<label for="linked_in">LinkedIn</label><br>
        		<input style="width:300px;" type="text" name="linked_in" id="linked_in" value="<?php echo get_option('linked_in'); ?>">
        	</p>
            <p>
                <label for="facebook">Facebook</label><br>
                <input style="width:300px;" type="text" name="facebook" id="facebook" value="<?php echo get_option('facebook'); ?>">
            </p>
             <p>
                <label for="twitter">Twitter</label><br>
                <input style="width:300px;" type="text" name="twitter" id="twitter" value="<?php echo get_option('twitter'); ?>">
            </p>
            <p>
                <label for="youtube">Youtube</label><br>
                <input style="width:300px;" type="text" name="youtube" id="youtube" value="<?php echo get_option('youtube'); ?>">
            </p>
        </div>
    
     <?php /*   <div class="company_networks">
            <h2>Businesss info</h2>
            <p>
                <label for="email_<?php echo ICL_LANGUAGE_CODE; ?>">E-mail</label><br>
                <input style="width:300px;" type="text" name="email_<?php echo ICL_LANGUAGE_CODE; ?>" id="email_<?php echo ICL_LANGUAGE_CODE; ?>" value="<?php echo get_option('email_'.ICL_LANGUAGE_CODE); ?>">
            </p>
             <p>
                <label for="tel_<?php echo ICL_LANGUAGE_CODE; ?>">Telphone</label><br>
                <input style="width:300px;" type="text" name="tel_<?php echo ICL_LANGUAGE_CODE; ?>" id="tel_<?php echo ICL_LANGUAGE_CODE; ?>" value="<?php echo get_option('tel_'.ICL_LANGUAGE_CODE); ?>">
            </p>
            <p>
                <label for="fax_<?php echo ICL_LANGUAGE_CODE; ?>">Fax</label><br>
                <input style="width:300px;" type="text" name="fax_<?php echo ICL_LANGUAGE_CODE; ?>" id="fax_<?php echo ICL_LANGUAGE_CODE; ?>" value="<?php echo get_option('fax_'.ICL_LANGUAGE_CODE); ?>">
            </p>

           <?php /*     <p>
                    <label for="location_<?php echo ICL_LANGUAGE_CODE; ?>">Location</label><br>
                    <textarea name="location_<?php echo ICL_LANGUAGE_CODE; ?>" id="location_<?php echo ICL_LANGUAGE_CODE; ?>" cols="30" rows="10" style="width: 300px;"><?php echo get_option('location_'.ICL_LANGUAGE_CODE); ?></textarea>
                </p> 
            
        </div>*/ ?>
           
        <!-- <div class="company-telephones">
            <h2>Telephones</h2>
            <p>
                <label for="tel_1">Telephone - 1 </label><br>
                <input style="width:300px;" type="text" name="tel_1" id="tel_1" value="<?php echo get_option('tel_1'); ?>">
            </p>
            <p>
                <label for="tel_2">Telephone - 2 </label><br>
                <input style="width:300px;" type="text" name="tel_2" id="tel_2" value="<?php echo get_option('tel_2'); ?>">
            </p>
            <p>
                <label for="tel_3">Telephone - 3 </label><br>
                <input style="width:300px;" type="text" name="tel_3" id="tel_3" value="<?php echo get_option('tel_3'); ?>">
            </p>

        </div> 

        <div class="company-emails">
            <h2>E-mails</h2>
            <p>
                <label for="info_email">Info Email</label><br>
                <input style="width:300px;" type="text" name="info_email" id="info_email" value="<?php echo get_option('info_email'); ?>">
            </p>
            <p>
                <label for="data_email">Email for reciving files</label><br>
                <input style="width:300px;" type="text" name="data_email" id="data_email" value="<?php echo get_option('data_email'); ?>">
            </p>
            <p>
                <label for="executive_email">Executive's E-amil</label><br>
                <input style="width:300px;" type="text" name="executive_email" id="executive_email" value="<?php echo get_option('executive_email'); ?>">
            </p>

        </div> -->
 <!-- <div class="company-address">
            <h2>Address</h2>
            <div class="address-editor" style="width:500px;">
                <?php 
                    $args = array(
                            'media_buttons' => false,
                            'teeny'         => true,
                        );
                    wp_editor( get_option('company_address_'.ICL_LANGUAGE_CODE),'company_address_'.ICL_LANGUAGE_CODE, $args );
                ?>
            </div>
        </div> -->


    </div>

    <div class="set-dates">
        <h2>Add & modify date for prices</h2>
        <?php $dates = get_option( 'the_price_date_'.ICL_LANGUAGE_CODE ); ?>
        <input type="text" name="add_price_date" id="add_price_date" placeholder="Write a date"> <a href="#" class="button button-primary" id="add-date-btn">Add Date</a>

        <div id="all-dates" style="margin-top: 20px;">
            <?php if( !empty( $dates ) ) : ?>
                <?php foreach( $dates as $date ) : ?>
                    <div class="the-date">
                        <input type="text" name="the_price_date_<?php echo ICL_LANGUAGE_CODE ?>[]" value="<?php echo $date; ?>" > <a href="#" class="button remove-date">Remove</a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>

        <script>
        jQuery(function(){


            jQuery( '#add-date-btn' ).click(function(){
                var $this    = jQuery( this ),
                    price    = jQuery( '#add_price_date' ),
                    priceVal = price.val();

                var result = '<div class="the-date"><input type="text" name="the_price_date_<?php echo ICL_LANGUAGE_CODE ?>[]" value="'+ priceVal +'" > <a href="#" class="button remove-date">Remove</a></div>';

                if( priceVal != '' ) {
                    jQuery('#all-dates').append( result );
                }

                price.val('');
                return false;
            });

            jQuery( '#all-dates' ).on( 'click', '.remove-date', function(){
                jQuery(this).closest( '.the-date' ).remove();
                return false;
            });
        });
        </script>
    </div>

    <p class="submit">
        <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
    </p>
</form>

<?php
}

/*END SUBHEADER OPTIONS   */