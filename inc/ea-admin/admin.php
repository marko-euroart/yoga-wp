<?php
// Custom admin style and functinalities for ea93

// Add EA93 Siganture into footer
function ea93_admin_copy($content){
	$content = ' Developed and maintaned by: <a href="http://www.euroart93.hr" traget="_blank">EuroArt93</a>. Powered by <a href="http://www.wordpress.org">WordPress.</a>';
	return $content;
}
add_filter('admin_footer_text', 'ea93_admin_copy');

// custom ea93 styles
function ea93_admin_styles()
{
	wp_register_style('ea93-style', get_template_directory_uri() . '/inc/ea-admin/admin-style.css');
	wp_enqueue_style('ea93-style');
}
add_action( 'admin_enqueue_scripts', 'ea93_admin_styles' );

function change_logo_url()
{
?>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			jQuery('#wp-admin-bar-wp-logo').find('a').attr('href', '<?php echo admin_url(); ?>');
		});

		var postsRND = jQuery('#dashboard_right_now').find('.b-posts a').text();
		var postsRNDTxt = jQuery('#dashboard_right_now').find('.posts a');
		
		if( postsRND > 1 ) {
			postsRNDTxt.text('Articles');
		} else {
			postsRNDTxt.text('Article');
		}
	</script>
<?php
}

add_action('admin_footer', 'change_logo_url');
// Change post label to "article"
function change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'Articles';
	$submenu['edit.php'][5][0] = 'Articles';
	$submenu['edit.php'][10][0] = 'Add Article';
	if( current_user_can( 'edit_others_pages' ) ) {
		$submenu['edit.php'][16][0] = 'Article Tags';
	}
	echo '';
}
function change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'Articles';
	$labels->singular_name = 'Article';
	$labels->add_new = 'Add Article';
	$labels->add_new_item = 'Add Article';
	$labels->edit_item = 'Edit Article';
	$labels->new_item = 'Article';
	$labels->view_item = 'View Article';
	$labels->search_items = 'Search Articles';
	$labels->not_found = 'No Articles found';
	$labels->not_found_in_trash = 'No Articles found in Trash';
	$labels->all_items = 'All Articles';
	$labels->menu_name = 'Articles';
	$labels->name_admin_bar = 'Article';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );

/**
* add excerpt to pages and change text to shortTxT
*
* @since 1.0
*/	
function mytheme_addbox() {
	add_meta_box('postexcerpt', __('Short Text'), 'post_excerpt_meta_box', 'page', 'normal', 'core');
	add_meta_box('postexcerpt', __('Short Text'), 'post_excerpt_meta_box', 'post', 'normal', 'core');
	//add_meta_box('postexcerpt', __('Short Text'), 'post_excerpt_meta_box', 'product', 'normal', 'core');
}
add_action( 'admin_menu', 'mytheme_addbox' );

/**
* add EA93 DASHBOARD WIDGET
*
* @since 1.0
*/	
// Create the function to output the contents of our Dashboard Widget
function example_dashboard_widget_function() {
	// Display whatever it is you want to show
	echo '<p>Need a support?<br> Give us a call: <strong>+385 44 533 931</strong>.<br>Mail us: <a href="#">euroart93@euroart93.hr</a></p>';
} 
// Create the function use in the action hook
function example_add_dashboard_widgets() {
	wp_add_dashboard_widget('example_dashboard_widget', 'Welcome to EA.WP', 'example_dashboard_widget_function');	
} 

// Hook into the 'wp_dashboard_setup' action to register our other functions
add_action('wp_dashboard_setup', 'example_add_dashboard_widgets' ); // Hint: For Multisite Network Admin Dashboard use wp_network_dashboard_setup instead of wp_dashboard_setup.


// IF CURRENT USER IS NOT #1 ADMIN
$current_user = wp_get_current_user();
if( $current_user->ID != 1 ) 
{	

	/**
	* Restrict non #1 users to admin locationas
	*
	* @since 1.0
	*/	
	function restrict_themes_locations()
	{
		if( ( contains_substr( 'wp-admin/themes.php', current_url() ) && !contains_substr( '?page', current_url()) ) ||  
			contains_substr( 'wp-admin/theme-editor.php', current_url() ) ||
			contains_substr( 'wp-admin/customize.php', current_url() ) ||
			contains_substr( 'wp-admin/update-core.php', current_url() ) || 
			contains_substr( 'wp-admin/plugins.php', current_url() ) || 
			contains_substr( 'wp-admin/plugin-install.php', current_url() ) ||
			contains_substr( 'wp-admin/plugin-editor.php', current_url() ) ) {
			wp_redirect( admin_url() );
		}
	}
	add_action( 'admin_init', 'restrict_themes_locations' );

	
	/**
	* Custom ea93 styles for regular user - non #1 usrer
	*
	* @since 1.0
	*/	
	function ea93_user_admin_styles()
	{
		wp_register_style('ea93-user-style', get_template_directory_uri() . '/inc/ea-admin/user-admin-style.css');
		wp_enqueue_style('ea93-user-style');	
	}
	add_action( 'admin_enqueue_scripts', 'ea93_user_admin_styles' );
	

	/**
	* Remove dashboards widgets from dashboard
	*
	* @since 1.0
	*/	
	function remove_dashboard_widgets() 
	{
		//remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
		// dashboard_recent_comments
	} 
	add_action('wp_dashboard_setup', 'remove_dashboard_widgets');



	/**
	* Remove unwanted menus and submenus
	*
	* @since 1.0
	*/
	function remove_menu_pages() 
	{
		remove_submenu_page( 'themes.php', 'theme-editor.php' );
		remove_submenu_page('index.php', 'update-core.php');
		remove_submenu_page('themes.php', 'widgets.php');
		remove_submenu_page('themes.php', 'customize.php');
		remove_submenu_page('themes.php', 'themes.php');
		remove_submenu_page('options-general.php', 'options-permalink.php');
		remove_submenu_page('options-general.php', 'options-discussion.php');
		remove_submenu_page('options-general.php', 'options-media.php');
		remove_menu_page('edit-comments.php');
		// remove_menu_page('tools.php');	
		remove_menu_page('link-manager.php');	
		remove_menu_page('plugins.php');	
		// remove_menu_page('sitepress-multilingual-cms/menu/languages.php');
	}
	add_action('admin_menu', 'remove_menu_pages' );

	// There is some bug with removing editor menu, need to use admin_init hook
	function remove_editor_submenu()
	{
		remove_submenu_page('themes.php', 'theme-editor.php');
		//remove_menu_page('sitepress-multilingual-cms/menu/languages.php');
	}
	add_action('admin_init', 'remove_editor_submenu');


	/**
	* Remove unwanted links from admin bar 
	*
	* @since 1.0
	*/
	function admin_bar_render() {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('comments');
		$wp_admin_bar->remove_menu('documentation');
		$wp_admin_bar->remove_menu('wporg');
		$wp_admin_bar->remove_menu('support-forums');
    	$wp_admin_bar->remove_menu('feedback');
    	$wp_admin_bar->remove_menu('about');
    	$wp_admin_bar->remove_menu('updates');
    	// $wp_admin_bar->remove_menu('wp-logo');

	}
	add_action( 'wp_before_admin_bar_render', 'admin_bar_render' );

}