<?php
/* 
* Admin part for product
*/
class Instructor extends Admin {

	public static $post_type = 'instructor';

	/*
		Construstor
	*/
	function __construct() 
	{
		// register post types and taxonomies
		add_action( 'init', array( $this, 'register_post_type' ) );
		//add_action( 'wp_enqueue_scripts', array( $this, 'eneque_custom_scripts' ) );
		//add_action( 'init', array( $this, 'create_taxonomies' ) );

		// load additional scripts and styles
		// add_action( 'admin_enqueue_scripts', array( $this, 'load_extra_scripts' ) );

		// add custom forms and metaboxes
		// add_action('add_meta_boxes', array( $this, 'load_mbox' ) );
		//add_action( 'edit_form_after_title', array( $this, 'subheading' ) );

		//save hooks for custom forms & fields
		// add_action( 'save_post', array( $this, 'save_testimonial_data' ) );

		//add_action( 'save_post', array( $this, 'save_prices_settings' ) );

	}

	/*
	*Register post_type
	*/
	public function register_post_type()
	{
		 $labels = array(
	    'name' => _x('Instructor', 'post type general name'),
	    'singular_name' => _x('Instructor', 'post type singular name'),
	    'add_new' => _x('Add New', 'Instructor'),
	    'add_new_item' => __('Add New Instructor'),
	    'edit_item' => __('Edit Instructor'),
	    'new_item' => __('New Instructor'),
	    'all_items' => __('All Instructors'),
	    'view_item' => __('View Instructor'),
	    'search_items' => __('Search Instructors'),
	    'not_found' =>  __('No Instructors found'),
	    'not_found_in_trash' => __('No Instructors found in Trash'), 
	    'parent_item_colon' => '',
	    'menu_name' => 'Instructors'

	  );
	  $args = array(
	  	'menu_icon' => 'dashicons-lightbulb',
	    'labels' => $labels,
	    'public' => false,
	    'publicly_queryable' => true,
	    'show_ui' => true, 
	    'show_in_menu' => true, 
	    'query_var' => true,
	    'rewrite' => true,
	    'has_archive' => false, 
	    'hierarchical' => true,
	    'menu_position' => null,
	    'supports' => array( 'title', 'editor', 'thumbnail')
	  ); 
	  register_post_type( self::$post_type, $args );
	}


	/* 
	Met a boxes
	*/
	// public function load_mbox() 
	// {
	// 	global $post, $sitepress;
	// 	add_meta_box( 'testimonial_settings1', 'Author & Location', array(  $this, 'testimonial_data' ), self::$post_type, 'normal', 'default' );
	// }


	// function testimonial_data(){
	// 	$this->load_view( 'view-data' );
	// 	wp_nonce_field( 'testimonial_settings1_noncename', 'testimonial_settings1_noncevalue' );
	// }

	// public function save_testimonial_data( $post_id ) {
	// 	if( postmeta_valid('testimonial_settings1_noncevalue', 'testimonial_settings1_noncename')  ) {
	// 		postmeta_save( 'testimonial_data' );
	// 		postmeta_save( 'testimonial_location' );
	// 	}
	// }

}
Instructor::get_instance();

/* End of file */
/* End of file class-product.php */