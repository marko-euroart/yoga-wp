<?php
/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 **/
//hook into the init action and call create_book_taxonomies when it fires
//add_action( 'init', 'create_programs_taxonomies', 0 );

//create two taxonomies, Categories and writers for the post type "book"
function create_programs_taxonomies() 
{
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name'                => _x( 'Categories', 'taxonomy general name' ),
    'singular_name'       => _x( 'Category', 'taxonomy singular name' ),
    'search_items'        => __( 'Search Categories' ),
    'all_items'           => __( 'All Categories' ),
    'parent_item'         => __( 'Parent Category' ),
    'parent_item_colon'   => __( 'Parent Category:' ),
    'edit_item'           => __( 'Edit Category' ), 
    'update_item'         => __( 'Update Category' ),
    'add_new_item'        => __( 'Add New Category' ),
    'new_item_name'       => __( 'New Category Name' ),
    'menu_name'           => __( 'Category' )
  );    

  $args = array(
    'hierarchical'        => true,
    'labels'              => $labels,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => _x( 'programs', 'URL slug', 'wp_starter' ) )
  );

  register_taxonomy( 'type', array( 'program' ), $args );

}


// Custom fileds in taxomomy
//loadat cemo neke skripte i stilove za taxonomije kako bi mogli korisiti media uploaer
function add_scripts_to_tax(){
  wp_enqueue_script('tax-media-uploader', get_template_directory_uri().'/js/media-uploader.js');

  //admin_print_styles
}
//add_action('admin_print_scripts-edit-tags.php', 'add_scripts_to_tax');

// Sejvamo dodatne meta fildove na taxomiji
function save_custom_meta( $term_id ) {
  if(isset($_POST['term_meta']['long_description'])) $_POST['term_meta']['long_description'] = stripslashes($_POST['term_meta']['long_description']);
  if ( isset( $_POST['term_meta'] ) ) {
    $t_id = $term_id;
    $term_meta = get_option( "taxonomy_$t_id" );
    $cat_keys = array_keys( $_POST['term_meta'] );
    foreach ( $cat_keys as $key ) {
      if ( isset ( $_POST['term_meta'][$key] ) ) {
        $term_meta[$key] = $_POST['term_meta'][$key];
      }
    }
    update_option( "taxonomy_$t_id", $term_meta );
  }
}  
add_action( 'edited_type', 'save_custom_meta', 10, 2 );  
add_action( 'create_type', 'save_custom_meta', 10, 2 );
add_action( 'edited_collection_type', 'save_custom_meta', 10, 2 );  
add_action( 'create_collection_type', 'save_custom_meta', 10, 2 );

add_action( 'edited_location', 'save_custom_meta', 10, 2 );  
add_action( 'create_location', 'save_custom_meta', 10, 2 );
add_action( 'edited_collection_location', 'save_custom_meta', 10, 2 );  
add_action( 'create_collection_location', 'save_custom_meta', 10, 2 );

// Dodajemo novi meta field kod kreiranja taxonimije
function tax_add_new_meta_field() {
?>
 <!-- <div class="form-field">
    <label for="term_meta[long_description]"><?php _e( 'Upload Image' ); ?></label>
    <input type="text" class="tax-input-image" name="term_meta[long_description]" id="term_meta[long_description]" style="width:70%">
    <a href="#" class="tax-upload-button button"><?php _e('Upload image'); ?></a>
  </div> -->
<?php
}
//add_action( 'type_add_form_fields', 'tax_add_new_meta_field', 10, 2 );
//add_action( 'collection_type_add_form_fields', 'tax_add_new_meta_field', 10, 2 );

// Dodajemo novi meta filed kod editiranja taxomije
function tax_edit_meta_field( $term ) {
  $t_id = $term->term_id;
  $term_meta = get_option( "taxonomy_$t_id" ); ?>

  <tr class="form-field">
  <th scope="row" valign="top">  <label for="term_meta[long_description]"><?php _e( 'Category content' ); ?></label></th>
    <td>
        <!-- <input type="text" class="tax-input-image" name="term_meta[long_description]" id="term_meta[long_description]" value="<?php echo $term_meta['long_description']; ?>" style="width:50%"> -->
        <div class="tax-editor" style="width:95%;">
        <?php 
            $args = array(
                    'media_buttons' => true,
                    'teeny'         => true,
                );
            wp_editor( $term_meta['long_description'], 'term_meta[long_description]', $args );
        ?>
      </div>
    </td>
  </tr>
   <tr class="form-field">
  <th scope="row" valign="top">  <label for="term_meta[long_description]"><?php _e( 'Category thumbnail' ); ?></label></th>
    <td>
      <div class="tax-thumb">
        <?php wp_enqueue_media(); ?>
        <?php postmeta_featured_img_tax( 'tax-thumb', $term ); ?>
      </div>
    </td>
  </tr>
<?php
}
add_action( 'type_edit_form_fields', 'tax_edit_meta_field', 10, 2 );
add_action( 'collection_type_edit_form_fields', 'tax_edit_meta_field', 10, 2 );
add_action( 'location_edit_form_fields', 'tax_edit_meta_field', 10, 2 );
add_action( 'collection_location_edit_form_fields', 'tax_edit_meta_field', 10, 2 );