<?php 
/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 * Ovaj API sluzi za lako dodavanje  i ažuriranja elemenata formi u post tipovima
 **/

// @since v1.0
// Dohvaca vrijednost post_meta, prvi argument je "meta" koji zelimo prikazati, a drugi argument je vrijednost "meta" ukoliko nije definirana vrijednost.
function get_postmeta_val($meta, $value="") {
	global $post;
    $values = get_post_custom($post->ID);
    $meta = isset($values[$meta]) ? esc_attr( $values[$meta][0] ) : $value;
    return trim($meta);
}

function get_postmeta_val_by_id($id, $meta, $value="") {
    $values = get_post_custom($id);
    $meta = isset($values[$meta]) ? esc_attr( $values[$meta][0] ) : $value;
    return trim($meta);
}
function postmeta_parse_attrs( $attrs ) {
	if( !is_array($attrs)  )
		return "";
	$result ="";
	$forbiden = array( 'name', 'id', 'value', 'type' );
	foreach ( $attrs as $key => $value ) {
		if( in_array($key, $forbiden) )
			continue;

		$result .= $key . '="'.$value.'" ';
	}
	return trim( $result );
} 

// @since v1.0
// Pregledava je li sve u redu prije nego sto ažuriramo "meta" vrijednosti. Uvijek koristiti ovu funckiju kada se spremaju meta vrijednosti
function postmeta_valid($nonce_name="", $nonce_value=""){
	global $post;
	if( !is_object( $post ) ) {
		return false;
	}
	if( !isset( $_POST[$nonce_name] ) ) {
		return false;
	}
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		{ return true; } 
	elseif( !wp_verify_nonce( $_POST[$nonce_name], $nonce_value ) )
		{ return true; } 
	elseif( !current_user_can( 'edit_post', $post->ID ) ) 
		{ return false; }
	else 
		{ return true;} 
}

function postmeta_radio( $name, $id ) {
?>
	<input type="radio" name="<?php echo $name ?>" id="<?php echo $id ?>" value="<?php echo $id ?>"  <?php checked( get_postmeta_val($name), $id ); ?> >
<?php
}

function postmeta_textarea( $id, $val="",$attr="") {
	global $postmeta_fields; 	
	$postmeta_fields['text'] = $id;
?>
	<textarea name="<?php echo $id ?>" id="<?php echo $id ?>" cols="30" rows="3" <?php echo postmeta_parse_attrs( $attr ); ?>><?php echo get_postmeta_val($id, $val); ?></textarea>
<?php
}

function postmeta_textbox( $id, $val="", $attr="" ){
?>
	<input type="text" name="<?php echo $id ?>" id="<?php echo $id ?>" value="<?php echo get_postmeta_val($id, $val); ?>" <?php echo postmeta_parse_attrs( $attr ); ?>>
<?php
}

function postmeta_upload_media($id, $val=""){
	global $post;
?>
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('.<?php echo $id; ?>-upload-button').click(function() {
		// spremamo originalnu send_to_editor() funckciju koju cemo iskoristiti ponovi
		var original_send_to_editor = window.send_to_editor;
		//formfield = jQuery('#document').attr('name');

		//prikazujemo media uploader
		tb_show('', 'media-upload.php?post_id=<?php echo $post->ID; ?>&TB_iframe=1&width=640&height=548');
		var tbframe_interval = setInterval(function() {jQuery('#TB_iframeContent').contents().find('.savesend .button').val('Use this file');}, 2000);

		// saljemo u ispravan input/textarea
		window.send_to_editor = function(html) {
			// vracamo nazad "Use this file" u "Insert into post"
			tbframe_interval = clearInterval(tbframe_interval);
	
			// href uzima sve pa i dokumente
			imgurl = jQuery(html).attr('href');
			// saljemo u #document 
			jQuery( '#<?php echo $id ?>-input-file' ).val(imgurl);
			//zatvarmo uploader
			tb_remove();
			// resetiramo send_to_editor na staro
			window.send_to_editor = original_send_to_editor;
		}
		return false;
	});

});
</script>
<input type="text" class="file-uplaod" name="<?php echo $id; ?>" id="<?php echo $id ?>-input-file" value="<?php echo get_postmeta_val($id); ?>" style="width:70%;padding:5px;">
<a href="#" class="button <?php echo $id; ?>-upload-button">Upload</a>
<?php
}

function postmeta_checkbox( $id, $val=1 ){
?>
	<input type="checkbox" <?php checked( get_postmeta_val($id), 1 ); ?> name="<?php echo $id ?>" id="<?php echo $id ?>" value="1">
<?php
}

function postmeta_dropdown( $id, $options ){
	?>

	<select name="<?php echo $id; ?>" id="<?php echo $id; ?>">
		<?php foreach( $options as $option => $text ) : ?>
			<option value="<?php echo $option; ?>" <?php selected( $option, get_postmeta_val($id) ); ?>><?php echo $text; ?></option>
		<?php endforeach; ?>
	</select>

<?php
}

function postmeta_featured_img( $name )
{
	global $post;
	wp_enqueue_media();
?>
	<div id="<?php echo $name ?>_container">
	<script type="text/javascript">
	jQuery(document).ready(function(){

		var file_frame;
		var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
		var set_to_post_id = <?php echo $post->ID; ?>; // Set this

		console.log( wp_media_post_id );

		// Remove featured img2
		jQuery('#remove_<?php echo $name ?>').live('click', function( event ){
			jQuery('#<?php echo $name ?>').val('');
			jQuery('#set_<?php echo $name ?>').show();
		 	jQuery('#remove_<?php echo $name ?>').hide();
		 	jQuery( '#<?php echo $name ?>_frame' ).empty();
			return false;
		});
		// Uploading files

		  jQuery('#set_<?php echo $name ?>, #set_<?php echo $name ?>_img').live('click', function( event ){
		 	
		    event.preventDefault();
		 
		    // If the media frame already exists, reopen it.
				    // If the media frame already exists, reopen it.
		    if ( file_frame ) {
		      // Set the post ID to what we want
		      file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
		      // Open frame
		      file_frame.open();
		      return;
		    } else {
		      // Set the wp.media post id so the uploader grabs the ID we want when initialised
		      wp.media.model.settings.post.id = set_to_post_id;
		    }
		 
		    // Create the media frame.
		    file_frame = wp.media.frames.file_frame = wp.media({
		      title: 'Set Featured Image',
		      button: {
		        text: 'Set Featured Image',
		      },
		      multiple: false,  // Set to true to allow multiple files to be selected
		      library: {
					type: 'image'
			  }
		    });
		 
		    // When an image is selected, run a callback.
		    file_frame.on( 'select', function() {
		      // We set multiple to false so only get one image from the uploader
		      attachment = file_frame.state().get('selection').first().toJSON();

		 		jQuery('#<?php echo $name ?>').val( attachment.id );
		 		jQuery('#set_<?php echo $name ?>').hide();
		 		jQuery('#remove_<?php echo $name ?>').show();
		 		jQuery( '#<?php echo $name ?>_frame' ).empty();
		 		jQuery( '<a href="#" id="set_<?php echo $name ?>_img"><img src="'+ attachment.url +'"" style="max-width:266px;margin-bottom:15px;"></a>' ).appendTo( '#<?php echo $name ?>_frame' );

		      // Do something with attachment.id and/or attachment.url here
		       // Restore the main post ID
      			wp.media.model.settings.post.id = wp_media_post_id;
		    });
		 
		    // Finally, open the modal
		    file_frame.open();
		  });

		});
		</script>	
	<?php $fimg = wp_get_attachment_url( get_postmeta_val( $name ) ); ?>
	<div id="<?php echo $name ?>_frame">
		<?php if( $fimg != false  ) : ?>
			<a id="set_<?php echo $name ?>_img" href="#"><img src="<?php echo wp_get_attachment_url( get_postmeta_val( $name ) ) ?>" style="max-width:266px;margin-bottom:15px;" ></a>
		<?php endif; ?>
	</div>
	<a href="#"  id="set_<?php echo $name ?>" <?php echo ( $fimg != false ? 'style="display:none;"' : '' ); ?> >Set featured image</a>
	<a href="#" id="remove_<?php echo $name ?>" <?php echo ( $fimg == false ? 'style="display:none;"' : '' ); ?> >Remove featured image</a>
	<input type="hidden" name="<?php echo $name ?>" id="<?php echo $name ?>" value="<?php echo get_postmeta_val( $name ); ?>">
</div>
<?php 
}

function has_post_thumb( $name, $id='' ){
	if( $id == '' ) {
		global $post;
		$id = $post->ID;	
	}
	
	$image_id = get_postmeta_val_by_id( $id, $name ) != '' ? get_postmeta_val_by_id( $id, $name ) : false;
	if( $image_id ) {
		$image_exists = wp_get_attachment_url( $image_id );
		if( !$image_exists ) {
			return false;
		} else {
			return true;
		}
	} else {	
		return false;
	}
}

function the_post_thumb( $name, $size="thumbnial", $attr = null ){
	global $post;
	if( !has_post_thumb( $name ) )
		return;
	$image_id = get_postmeta_val_by_id( $post->ID, $name );
	echo wp_get_attachment_image( $image_id, $size, false, $attr );
}

function get_the_post_thumb( $id, $name, $size="thumbnial", $attr = null ){
	if( !has_post_thumb( $name, $id ) )
		return;
	$image_id = get_postmeta_val_by_id( $id, $name );
	return wp_get_attachment_image( $image_id, $size, false, $attr );
}


function postmeta_save($name){
	global $post_id;
	$name_p = isset( $_POST["$name"] ) ? $_POST["$name"] : "" ;
	if( isset( $name_p ) ) {
		update_post_meta( $post_id, $name, $name_p );
	} else {
		update_post_meta( $post_id, $name, '' );
	}
}

function postmeta_save_checkbox($name){
	global $post_id;
	$name_p = $_POST[$name];
	if( isset($name_p) ) {
		update_post_meta( $post_id, $name, 1 );
	} else {
		update_post_meta( $post_id, $name, 0 );
	}
}


function postmeta_uploader( $id, $val="" )
{
	global $post;
?>
	<div id="<?php echo $id ?>_container">
	<script type="text/javascript">
	jQuery(document).ready(function(){

		var file_frame;
		var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
		var set_to_post_id = '<?php echo $post->ID; ?>'; // Set this

		console.log( wp_media_post_id );

		  jQuery('#set_<?php echo $id ?>').live('click', function( event ){
		 	
		    event.preventDefault();
		 
		    // If the media frame already exists, reopen it.
				    // If the media frame already exists, reopen it.
		    if ( file_frame ) {
		      // Set the post ID to what we want
		      file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
		      // Open frame
		      file_frame.open();
		      return;
		    } else {
		      // Set the wp.media post id so the uploader grabs the ID we want when initialised
		      wp.media.model.settings.post.id = set_to_post_id;
		    }
		 
		    // Create the media frame.
		    file_frame = wp.media.frames.file_frame = wp.media({
		      title: 'Upload Media',
		      button: {
		        text: 'Use this file',
		      },
		      multiple: false,  // Set to true to allow multiple files to be selected
		     /* library: {
					type: 'image'
			  }*/
		    });
		 
		    // When an image is selected, run a callback.
		    file_frame.on( 'select', function() {
		      // We set multiple to false so only get one image from the uploader
		      attachment = file_frame.state().get('selection').first().toJSON();
		      console.log( attachment );
		      
		      jQuery('#<?php echo $id ?>').attr( 'value', attachment.url );
		      // Do something with attachment.id and/or attachment.url here
		       // Restore the main post ID
      			wp.media.model.settings.post.id = wp_media_post_id;
		    });
		 
		    // Finally, open the modal
		    file_frame.open();
		  });

		});
		</script>	
		<input type="text" name="<?php echo $id ?>" id="<?php echo $id ?>" value="<?php echo get_postmeta_val($id, $val); ?>" style="width: 50%" >
		<a href="#" class="button primary-button" id="set_<?php echo $id ?>" >Upload Media</a>
</div>
<?php 
}


function postmeta_featured_img_tax( $name, $term )
{
	global $post;
	$t_id = $term->term_id;
  	$term_meta = get_option( "taxonomy_$t_id" );
	wp_enqueue_media();
?>
	<div id="<?php echo $name ?>_container">
	<script type="text/javascript">
	jQuery(document).ready(function(){

		var file_frame;
		var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
		var set_to_post_id = '<?php echo $post->ID; ?>'; // Set this

		console.log( wp_media_post_id );

		// Remove featured img2
		jQuery('#remove_<?php echo $name ?>').live('click', function( event ){
			jQuery('#<?php echo $name ?>').val('');
			jQuery('#set_<?php echo $name ?>').show();
		 	jQuery('#remove_<?php echo $name ?>').hide();
		 	jQuery( '#<?php echo $name ?>_frame' ).empty();
			return false;
		});
		// Uploading files

		  jQuery('#set_<?php echo $name ?>, #set_<?php echo $name ?>_img').live('click', function( event ){
		 	
		    event.preventDefault();
		 
		    // If the media frame already exists, reopen it.
		 	// If the media frame already exists, reopen it.
		    if ( file_frame ) {
		      // Set the post ID to what we want
		      file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
		      // Open frame
		      file_frame.open();
		      return;
		    } else {
		      // Set the wp.media post id so the uploader grabs the ID we want when initialised
		      wp.media.model.settings.post.id = set_to_post_id;
		    }
		 
		    // Create the media frame.
		    file_frame = wp.media.frames.file_frame = wp.media({
		      title: 'Set Featured Image',
		      button: {
		        text: 'Set Featured Image',
		      },
		      multiple: false,  // Set to true to allow multiple files to be selected
		      library: {
					type: 'image'
			  }
		    });
		 
		    // When an image is selected, run a callback.
		    file_frame.on( 'select', function() {
		      // We set multiple to false so only get one image from the uploader
		      attachment = file_frame.state().get('selection').first().toJSON();

		 		jQuery('#<?php echo $name ?>').val( attachment.id );
		 		jQuery('#set_<?php echo $name ?>').hide();
		 		jQuery('#remove_<?php echo $name ?>').show();
		 		jQuery( '#<?php echo $name ?>_frame' ).empty();
		 		jQuery( '<a href="#" id="set_<?php echo $name ?>_img"><img src="'+ attachment.url +'"" style="max-width:266px;margin-bottom:15px;"></a>' ).appendTo( '#<?php echo $name ?>_frame' );

		      // Do something with attachment.id and/or attachment.url here
		       // Restore the main post ID
      			wp.media.model.settings.post.id = wp_media_post_id;
		    });
		 
		    // Finally, open the modal
		    file_frame.open();
		  });

		});
		</script>	
		<?php $img_id = $term_meta[ $name ]; ?>
	<?php $fimg = wp_get_attachment_url( $term_meta[ $name ] ); ?>
	<div id="<?php echo $name ?>_frame">
		<?php if( $fimg != false  ) : ?>
			<a id="set_<?php echo $name ?>_img" href="#"><img src="<?php echo $fimg; ?>" style="max-width:266px;margin-bottom:15px;" ></a>
		<?php endif; ?>
	</div>
	<a href="#" class="button" id="set_<?php echo $name ?>" <?php echo ( $fimg != false ? 'style="display:none;"' : '' ); ?> >Set featured image</a>
	<a href="#" class="button" id="remove_<?php echo $name ?>" <?php echo ( $fimg == false ? 'style="display:none;"' : '' ); ?> >Remove featured image</a>
	<input type="hidden" name="term_meta[<?php echo $name ?>]" id="<?php echo $name ?>" value="<?php echo $term_meta[ $name ]; ?>">
</div>
<?php 
}