<?php  
/* euroART93 custom login options */
function enqueue_login_styles()
{
	wp_enqueue_style('ea93-login', get_template_directory_uri() . '/inc/ea-login/login-style.css', false, '1'); 
}
add_action( 'login_enqueue_scripts', 'enqueue_login_styles' );

// Cutom URL for login page
add_filter( 'login_headerurl', 'ea93_login_url' );
function ea93_login_url($url) {
	return 'http://www.euroart93.hr';
}