<?php
/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 * Exstenzija za usera, dodajemo cusotm user upload slike itd
**/

// DODAJEMO DODATNE STVARI POD KONTAKTE //
function extra_contact_info($contactmethods) 
{
    $contactmethods['twitter'] =  'Twitter';
    $contactmethods['facebook'] = 'Facebook';
    $contactmethods['linkedin'] = 'LinkedIn';
    $contactmethods['googleplus'] = 'Google +';
  //  $contactmethods['speakerdeck'] = 'SpeakerDeck';
    return $contactmethods;
}
add_filter('user_contactmethods', 'extra_contact_info');
