<?php 
/* Post query */ 
?>
<?php 
$q = new WP_Query( array( 'posts_per_page' => -1, 'post_type' => 'post', 'order' => 'ASC' ) ); 
?>
<?php if( $q->have_posts() ) : while( $q->have_posts() ) : $q->the_post(); ?>
<?php if( has_post_thumbnail() ) : ?>
<?php the_post_thumbnail( 'pool-thumb'); ?>
<?php endif; ?>
<?php the_title(); ?>
<?php echo get_post_permalink(); ?>
<?php endwhile; endif; wp_reset_postdata(); ?>
<?php 
/* Post query */ 
?>

<?php 
/* Single page loop */ 
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
<?php 
/* Single page loop */ 
?>


<?php 
/* Slider primjer */ 
?>
<?php
$q = new WP_Query( array( 'posts_per_page' => -1, 'post_type' => 'slider',  'order' => 'ASC' ) ); ?>

<?php if( $q->have_posts() ) : ?>
<div class="slider-imgs">
	<?php $c=1; while( $q->have_posts() ) : $q->the_post(); ?>
		<?php if( has_post_thumbnail() ) : ?>
			<?php $current = $c == 1 ? ' current-showing' : ''; ?>
			<span><?php the_post_thumbnail( 'slider-img', array( 'class' => "slide{$c} $current" ) ); ?></span>
		<?php $c++; endif; ?>
	<?php endwhile; ?>
</div> 
<?php endif; wp_reset_postdata(); ?>

<?php if( $q->have_posts() ) : ?>
<div class="slider">
	<div class="slider-wrapper">
	<?php $c=1; while( $q->have_posts() ) : $q->the_post(); ?>
		<div class="slide <?php echo get_postmeta_val( 'slider_color' ); ?>" data-slide="slide<?php echo $c ?>">
				<h1><?php echo get_the_content(); ?></h1>
				<h2><a href="<?php echo get_postmeta_val( 'slider_link' ); ?>">
						<?php echo get_postmeta_val( 'slider_link_text' ); ?>
					</a>
				</h2>
		</div>
	<?php $c++; ?>
	<?php endwhile; ?>
</div>
<?php endif; wp_reset_postdata(); ?>
<?php 
/* Slider primjer */ 
?>