<footer id="page-footer" role="contentinfo">
	<?php 
	$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
	if ( $myaccount_page_id ): ?>
	  
	<section id="join-joga">
		<div class="container user-content">
			<h2 class="title-5">Postanite član <span>Padma yoga Centra</span> već danas!</h2>
			<?php /* <p class="highlighted-2">Već od <span>25 EUR</span> mjesečno!</p> */ ?>

			<a href="<?php echo get_permalink( $myaccount_page_id ); ?>" class="btn-2">Postanite član</a>
		</div>
	</section>

	<?php endif; ?>

	

	<?php
		$frontpage_id = get_option('page_on_front');
		$frontpage_id = wpml_id($frontpage_id);

		$newsletter_title = get_field('newsletter_title', $frontpage_id);
		$newsletter_text = get_field('newsletter_text', $frontpage_id);
		$address = get_field('address', $frontpage_id); 
		$phone = get_field('phone', $frontpage_id); 

		$facebook = get_field_object('facebook', $frontpage_id); 
		$youtube = get_field_object('youtube', $frontpage_id); 
		$instagram = get_field_object('instagram', $frontpage_id); 
		$google = get_field_object('google', $frontpage_id); 
		$twitter = get_field_object('twitter', $frontpage_id); 

		$socials = array( $facebook, $youtube, $instagram, $google, $twitter ); 

		$disclamer = get_field('disclamer', $frontpage_id); 
	?>
	<section id="newsletter">
		<div class="container">

			<?php if($newsletter_title): ?>
			<div class="title-1">
				<?php echo title_span($newsletter_title); ?>
			</div>
			<?php endif; ?>

			<?php if($newsletter_text) {
				echo '<p>'. $newsletter_text .'</p>';
			} ?>
			
			<form action="#" method="#" class="newsletter-form">
				<div class="group">
					<input type="text" placeholder="Vaš Email ...">
					<button type="submit" class="check-btn"></button>
				</div>
			</form>
			<div class="footer-content">
				<?php if($address): ?>
				<div class="block">
					<h2 class="highlighted"><?php echo bloginfo('name'); ?></h2>
					<?php echo $address; ?>
				</div>
				<?php endif; ?>

				<?php if($phone): ?>
				<div class="block">
					<?php echo $phone; ?>
				</div>
				<?php endif; ?>

				<div class="block">
					<div class="highlighted"><?php _e('Pronađite nas i na:', 'yoga'); ?></div>
					<ul class="socials">
						<?php 
							if( !empty($socials) ) {
								foreach ($socials as $social) {
									if( !empty($social['value']) ) {
										echo '<li class="'. $social['name'] .'"><a href="'. $social['value'] .'" target="_blank">'. $social['name'] .'</a></li>';
									}
								}
							}
						?>
					</ul>
				</div>
			</div>
			<div class="disclaimer">
				<p>
				<?php _e('Copyright', 'yoga'); ?> &copy; <?php echo date('Y'); ?> <?php echo bloginfo('name'); ?><br> 
				<?php if($disclamer) {
					echo '<span>'. $disclamer .'</span>';
				} ?>
				</p>
				<nav>
					<?php wp_nav_menu( menu_args( 'secondary_menu' ) ); ?>
				</nav>
			</div>
		</div>
	</section>
</footer>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73826441-1', 'auto');
  ga('send', 'pageview');

</script>

<?php wp_footer(); ?>
</body>
</html>