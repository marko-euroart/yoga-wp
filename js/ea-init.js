var EA = window.EA || {};
EA.Methods = {};
EA.LoadMethod = function(context){
	if( context === undefined ) {
		context = $(document);
	}

	context.find( '*[data-method]' ).each(function(){
		var that 		= $(this),
		    methods 	= that.attr('data-method');

		$.each(methods.split(" "), function(index, methodName){
			try {
				var MethodClass 	  = EA.Methods[methodName],
				    initializedMethod = new MethodClass(that);
			}
			catch(e) {
				// blank
			}
		});
	});
};

EA.onReady = function(){
	EA.LoadMethod();
	$('html').removeClass('no-js').addClass('js');
};

$(document).ready(function(){
	EA.onReady();
});

