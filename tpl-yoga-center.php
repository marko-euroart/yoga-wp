<?php 
//Template name: Yoga Center
get_header(); ?>

<?php 
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header-image' );
	$style = '';
	if( !empty($thumb) ) {
		$url = $thumb['0']; 
		$style = 'style="background: url('. $url .') no-repeat center top"';
	}
?>

<div id="promo" class="yoga-center" <?php echo $style; ?>>
	<div class="container">
		<h1 class="title-3">
			<?php the_title(); ?>
			<?php if( get_field('subtitle') ) {
				echo '<span>'. get_field('subtitle') .'</span>';
			} ?>
		</h1>
	</div>
</div>

<?php 
	$intro_left = get_field('intro_left');
	$intro_right = get_field('intro_right');
	$images = get_field('gallery');
?>
<div id="main" class="yc">
	<div class="container">
		<div class="about">
		<?php if($intro_left): ?>
			<div class="left-side">
				<blockquote><?php echo $intro_left; ?></blockquote>
			</div>
		<?php endif; ?>
		<?php if($intro_right): ?>
			<div class="right-side user-content">
				<?php echo $intro_right; ?>
			</div>
		<?php endif; ?>
		</div>


		<?php get_template_part('partials/video-section'); ?>


		<div class="article-detailed">
			<?php if( $images ): ?>
			<div class="gallery" data-method="gallery">
				<?php foreach( $images as $image ): ?>
					<div class="thumb">
						<a class="fancybox" rel="group" href="<?php echo $image['url']; ?>">
							<img src="<?php echo $image['sizes']['gallery-thumb']; ?>" alt="<?php echo $image['alt']; ?>"/>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
			<?php endif; ?>

			<div class="article-content">
				<article class="user-content">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				</article>
			</div>
		</div>
	</div>
</div>	

<?php get_footer(); ?>