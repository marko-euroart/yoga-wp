<?php 
//Template name: Benefits
get_header(); ?>

<?php 
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header-image' );
	$style = '';
	if( !empty($thumb) ) {
		$url = $thumb['0']; 
		$style = 'style="background: url('. $url .') no-repeat center bottom"';
	}
?>
<div id="promo" class="home-benificije" <?php echo $style; ?>>
	<div class="container">
		<h1 class="title-3">
			<?php the_title(); ?>
			<?php if( get_field('subtitle') ) {
				echo '<span>'. get_field('subtitle') .'</span>';
			} ?>
		</h1>
	</div>
</div>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php
	$left_content = get_field('left_content');
	$right_content = get_field('right_content');
	$big_title = get_field('big_title');
	$benefit_title = get_field('benefit_title');
	$benefit_list = get_field('benefit_list');
	$text_title = get_field('text_title');
	$text_field = get_field('text_field');
	$break_section_title = get_field('break_section_title');
	$break_section_text = get_field('break_section_text');
?>
<div id="main">
	<section id="benificije">
		<div class="container">

			<?php if($left_content): ?>
			<div class="left-side upper">
				<p class="highlighted"><?php echo $left_content; ?></p>
			</div>
			<?php endif; ?>
			<?php if($right_content): ?>
			<div class="right-side upper">
				<article class="user-content">
					<p><?php echo $right_content; ?></p>
				</article>
			</div>
			<?php endif; ?>

			<?php if($big_title): ?>
			<div class="full-width-wrapper">
				<h2 class="big-title"><?php echo $big_title; ?></h2>
			</div>
			<?php endif; ?>

			<div class="left-side user-content">
				<?php if($benefit_title): ?>
				<h3 class="highlighted"><?php echo $benefit_title; ?></h3>
				<?php endif; ?>


				<?php
				if( have_rows('benefit_list') ):
					echo '<ol highlighted-ol>';
				    while ( have_rows('benefit_list') ) : the_row();
						echo '<li><p><strong>'. get_sub_field('title') .'</strong></p><p>'. get_sub_field('description') .'</p></li>';
				    endwhile;
				    echo '</ol>';
				endif;
				?>


			</div>
			<div class="right-side">
				<article class="user-content">
				<?php if($text_title): ?>
					<h4 class="highlighted"><?php echo $text_title; ?></h4>
				<?php endif; ?>
				<?php 
					if($text_field) {
						echo $text_field; 
					}
				?>
				</article>
			</div>

		</div>
	</section>

	<section id="user-warning">
		<div class="container">
		<?php if($break_section_title): ?>
			<div class="left-side">
				<article class="user-content">
					
					<?php echo $break_section_title; ?>
				</article>
			</div>
		<?php endif; ?>
		<?php if($break_section_text): ?>
			<div class="right-side">
				<article class="user-content">
					<?php echo $break_section_text; ?>
				</article>
			</div>
		<?php endif; ?>
		</div>
	</section>

</div>
<?php endwhile; endif; ?>

<?php get_footer(); ?>