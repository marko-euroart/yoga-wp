<section id="popular-videos">
	<div class="container">
		<div class="video-blocks">
			<h2 class="title-5">Popularno među korisnicima</h2>
			<div class="video-blocks-wrapper">
				
				<?php echo do_shortcode( '[best_selling_products per_page="3"]' ); ?>
				
			</div>
		</div>
	</div>
</section>