<?php 
//Template name: Privacy
get_header(); ?>

<?php 
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header-image' );
	$style = '';
	if( !empty($thumb) ) {
		$url = $thumb['0']; 
		$style = 'style="background: url('. $url .') no-repeat center bottom"';
	}
?>

<div id="promo" class="privacy" <?php echo $style; ?>>
	<div class="container">
		<h1 class="title-3">
			<?php the_title(); ?>
			<?php if( get_field('subtitle') ) {
				echo '<span>'. get_field('subtitle') .'</span>';
			} ?>
		</h1>
	</div>
</div>

<div id="main">
	<section id="privacy">
		<div class="container">
			<article class="user-content privacy-content">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</article>
		</div>
	</section>
</div>

<?php get_footer(); ?>