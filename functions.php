<?php
/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 **/
require( get_template_directory() . '/inc/init.php' );
add_action( 'after_setup_theme', 'wp_starter_setup' );


/***************************/
// If not using WPML
/***************************/
if( !isset($sitepress) ) {
	$sitepress = false;
}
if( !defined('ICL_LANGUAGE_CODE') ) {
	define( 'ICL_LANGUAGE_CODE', false );
}
// end if not using WPML


if ( ! function_exists( 'wp_starter_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function wp_starter_setup() {

	load_theme_textdomain( 'wp_starter', get_template_directory() . '/languages' );

	add_editor_style();
	add_ea93_login();
	add_ea93_admin();

	//load_helper( array('date-time', 'string', 'statistics', 'images', 'navigation') );
	load_helper( array('media', 'string', 'date-time', 'navigation' ) );
	load( array( 'post-meta-api', 
				 'theme-options',
				 'ajax-calls',
				 'behaviors',
			) );

	load_class( 
		array( 
			'class-admin', 
			'class-front' ,
			'class-instructor'
		) );

	// Add default posts and comments RSS feed links to <head>.
	add_theme_support( 'automatic-feed-links' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Primary Menu' ) );
	register_nav_menu( 'secondary', __( 'Secondary Menu') );
	register_nav_menu( 'tertiary', __( 'Tertiary Menu') );
	register_nav_menu( 'quaternary', __( 'Quaternary Menu') );

	// remove menu container div
	function my_wp_nav_menu_args( $args = '' )
	{
	    $args['container'] = false;
	    return $args;
	} // function
	add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );

	// Add support for a variety of post formats
	// add_theme_support( 'post-formats', array( 'aside', 'link', 'gallery', 'status', 'quote', 'image' ) );

	// This theme uses Featured Images (also known as post thumbnails) for per-post/per-page Custom Header images
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'instructor-thumb', 170, 250, true );
	add_image_size( 'instructor-thumb-small', 85, 85, true );
	add_image_size( 'cert-image', 200, 200, true );
	add_image_size( 'header-image', 1920, 740, true );
	add_image_size( 'gallery-thumb', 170, 130, true );
	add_image_size( 'product-thumb', 370, 280, true );
}
endif; // wp_starter_setup


if( !is_admin() ){
	add_action('wp_enqueue_scripts', 'wp_starter_default_scripts');
}
function wp_starter_default_scripts() {

	wp_deregister_script( 'jquery' );

	wp_register_script( "jquery", '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', array(), '1.9.1', false );
	// wp_register_script( "script", get_template_directory_uri().'/js/script-min.js', array(), '1.0', true );
	wp_register_script( "script", get_template_directory_uri().'/js/script.js', array(), '1.0', true );

	// if using ajax
	//wp_localize_script( 'script', 'my_ajax', array('ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	wp_enqueue_script('jquery');
	wp_enqueue_script('script');

}
function wp_starter_excerpt_length( $length ) {
	return 38;
}
add_filter( 'excerpt_length', 'wp_starter_excerpt_length' );

function wp_starter_continue_reading_link() {
	return "";
}

function wp_starter_auto_excerpt_more( $more ) {
	return ' &hellip;' . wp_starter_continue_reading_link();
}
add_filter( 'excerpt_more', 'wp_starter_auto_excerpt_more' );


function wp_starter_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= wp_starter_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'wp_starter_custom_excerpt_more' );

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
function wp_starter_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'wp_starter_page_menu_args' );

function content_nav( $nav_id ="pagination" ) {
	global $wp_query;

	if ( $wp_query->max_num_pages > 1 ) : ?>
	        <li class="<?php echo $nav_id; ?>">
                <ul>
                    <li class="prev"><?php next_posts_link( __('next') ); ?></li>
                    <li class="next"><?php previous_posts_link( __('prev') ); ?></li>
                </ul>
            </li>
	<?php endif;
}


function wp_starter_body_classes( $classes ) {

	if ( function_exists( 'is_multi_author' ) && ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_singular() && ! is_home() && ! is_page_template( 'showcase.php' ) && ! is_page_template( 'sidebar-page.php' ) )
		$classes[] = 'singular';

	return $classes;
}
add_filter( 'body_class', 'wp_starter_body_classes' );


// dodajemo excerpt support i u page
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
    add_post_type_support( 'page', 'excerpt' );
}

class Custom_Walker_Class extends Walker_Nav_Menu {

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names .'>';


		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'><span class="decor"></span>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

}


// menu stuff
function no_menu(){ return ""; }
function menu_args($menu){
	// class="%2$s"
	$primary_menu = array(
	  'theme_location'  => "primary",
	  'menu'            => "", 
	  'container'       => '', 
	  'container_class' => 'menu-{menu slug}-container', 
	  'container_id'    => "",
	  'menu_class'      => 'main-nav', 
	  'menu_id'         => "example",
	  'echo'            => true,
	  'fallback_cb'     => 'no_menu',
	  'before'          => "",
	  'after'           => "",
	  'link_before'     => "",
	  'link_after'      => "",
	  'items_wrap'      => '<ul>%3$s</ul>',
	  'depth'           => 0,
	  'walker'          => ''
	);

	$secondary_menu = array(
	  'theme_location'  => "secondary",
	  'menu'            => "", 
	  'container'       => '', 
	  'container_class' => 'menu-{menu slug}-container', 
	  'container_id'    => "",
	  'menu_class'      => 'grid-8 about-menu', 
	  'menu_id'         => "additional-menu-ul",
	  'echo'            => true,
	  'fallback_cb'     => 'no_menu',
	  'before'          => "",
	  'after'           => "",
	  'link_before'     => "",
	  'link_after'      => "",
	  'items_wrap'      => '<ul>%3$s</ul>',
	  'depth'           => 0,
	  'walker'          => ''
	);

	$tertiary_menu = array(
	  'theme_location'  => "tertiary",
	  'menu'            => "", 
	  'container'       => '', 
	  'container_class' => 'menu-{menu slug}-container', 
	  'container_id'    => "",
	  'menu_class'      => 'footer-navigation', 
	  'menu_id'         => "footer-nav",
	  'echo'            => true,
	  'fallback_cb'     => 'no_menu',
	  'before'          => "",
	  'after'           => "",
	  'link_before'     => "",
	  'link_after'      => "",
	  'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
	  'depth'           => 0,
	  'walker'          => ""
	);

	$quaternary_menu = array(
	  'theme_location'  => "quaternary",
	  'menu'            => "", 
	  'container'       => '', 
	  'container_class' => 'menu-{menu slug}-container', 
	  'container_id'    => "",
	  'menu_class'      => 'subfooter-menu', 
	  'menu_id'         => "subfooter",
	  'echo'            => true,
	  'fallback_cb'     => 'no_menu',
	  'before'          => "",
	  'after'           => "",
	  'link_before'     => "",
	  'link_after'      => "",
	  'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
	  'depth'           => 0,
	  'walker'          => ""
	);

	return $$menu;
}


// Provjerava postoji li tablica, mora se pozvati i prenjetei ime 
function table_exists($table_name){
	global $wpdb;
	$result = $wpdb->query("SHOW TABLES LIKE '".$table_name."'");
	if( $result > 0 ) { return true; } else { return false; }
}



// wpml list langugae
function switch_language(){
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) {
	    $languages = icl_get_languages('skip_missing=0&orderby=id');
		//array_reverse($languages);
		//print_r( $languages );
	    if(!empty($languages)){
	    	$lngs = array_reverse( $languages );
	        foreach($lngs as $l) {
        		$active = $l['active'] == 1 ? " active" : "";
        		if(  $l['active'] != 1 ) {
        			return $l;
        		}
	        }
	    }
    }
}

/*function change_lang()
{
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) 
	{
		$lang_str = ICL_LANGUAGE_CODE == 'hr' ? 'PROMIJENI JEZIK:' : 'Change Language:';
		$languages = icl_get_languages('skip_missing=0&orderby=id&order=ASC');
		if( !empty( $languages ) )
		{
				foreach( $languages as $lang )
				{
					$active = $lang['active'] == 1 ? "active" : "";
					$language_code = $lang['language_code'];
					$language_text = $lang['language_code'];
					$lang_txt = str_replace( 'en' , 'eng', $language_code );
					echo  '<a class="'. $lang_txt .' '. $active .'" href="'. $lang['url'] .'">'. strtoupper($lang_txt) .'</a>';
				}
		}
	}
}*/

function change_lang()
{
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) 
	{
		$lang_str = ICL_LANGUAGE_CODE == 'hr' ? 'PROMIJENI JEZIK:' : 'Change Language:';
		$languages = icl_get_languages('skip_missing=0&orderby=id&order=ASC');
		if( !empty( $languages ) ) {
			echo ' <ul class="lang-nav">';
				foreach( $languages as $lang ) {
					$active = $lang['active'] == 1 ? " current" : "";
					$language_code = $lang['language_code'];
					$lang_txt = str_replace( 'en' , 'eng', $language_code );
					$lang_txt = str_replace( 'hr' , 'cro', $language_code );
					echo '<li class="' . $lang_txt . '' . $active. '"><span class="icon"></span><a href="' . $lang['url'] . '">'.$lang_txt.'</a></li>';
				}
			echo ' </ul>';
		}
	}
}

// Dinamciki gledamo nativno ime za lang_code. Uzimamo ime tocno iz WPML tablice.
function get_native_lang_name( $lang_code ) {
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) {
		global $wpdb;
		$t_name = $wpdb->prefix.'icl_languages';
		$result = $wpdb->get_var("SELECT english_name FROM $t_name WHERE code = '$lang_code'");
		return $result;
	}
}

// Dinamicki gleda sve permalinkove vezane za neku stranicu sa wpml, potrebno je ipak staviti post_ID od originalnog clanka
function page_uri( $pid ) {
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) {
		global $wpdb;
		$t_name = $wpdb->prefix.'icl_translations';
		$current_lang = ICL_LANGUAGE_CODE;
		$trid =  $wpdb->get_var("SELECT trid FROM $t_name WHERE element_id = $pid LIMIT 1 ");
		$page_id = $wpdb->get_var("SELECT element_id FROM $t_name WHERE trid = $trid AND language_code = '$current_lang' LIMIT 1 ");

		if( $page_id != "" ) {
			$result = get_permalink($page_id);
		} else {
			$result = "#";
		}
		echo $result;
	} else {
		echo "#";
	}
}

function wpml_id( $pid, $element_type = "" ){
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) {
		global $wpdb;

		$elem_string = $element_type != "" ? " AND element_type = '". $element_type ."' " : "";

		$t_name = $wpdb->prefix.'icl_translations';
		$current_lang = ICL_LANGUAGE_CODE;
		$trid =  $wpdb->get_var("SELECT trid FROM $t_name WHERE element_id = $pid $elem_string LIMIT 1");
		$page_id = $wpdb->get_var("SELECT element_id FROM $t_name WHERE trid = $trid AND language_code = '$current_lang' LIMIT 1 ");

		return $page_id;
	} else {
		return $pid;
	}
}

function wpml_id_lang( $pid, $element_type ="post", $lang ="" )
{
	if( $lang == "" ) {
		$lang = ICL_LANGUAGE_CODE;
	}
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) {
		global $wpdb;
		$t_name = $wpdb->prefix.'icl_translations';
		$current_lang = ICL_LANGUAGE_CODE;
		$trid =  $wpdb->get_var("SELECT trid FROM $t_name WHERE element_id = $pid AND element_type LIKE '%{$element_type}%' LIMIT 1 ");
		$page_id = $wpdb->get_var("SELECT element_id FROM $t_name WHERE trid = $trid AND language_code = '{$lang}' LIMIT 1 ");

		return $page_id;
	} else {
		return $pid;
	}
}

// Current lang u principu za oznacvanje <html lang-a>
function set_current_lang($def="en") {
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) :
		echo ICL_LANGUAGE_CODE;
	else:
		echo $def;
	endif;
}

// Neke postove jednostavno zelimo usmjeriti prema hrvatskom postu jer su jezik ful slican i svi ga razumiju.
function get_post_passed_url_wmpl($post_id){
	if( defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != "" ) :
		global $wpdb, $post;
		$current_lang = ICL_LANGUAGE_CODE;
		//init result, ostaje prazan ako nista ne nade
		$result = "";

		$t_name = $wpdb->prefix.'icl_translations';
		$trid   = $wpdb->get_var("SELECT trid FROM $t_name WHERE element_id = $post_id LIMIT 1");
		
		$checked_lang =  get_postmeta_val('url_overwrite');
		if ( $checked_lang != "" || $checked_lang != 'none_url_overwrite' ){

			$passed_post_id = $wpdb->get_var("SELECT element_id FROM $t_name WHERE trid = $trid AND language_code = '$checked_lang' LIMIT 1");
			$result = $passed_post_id;
			
		} 
		return $result;
	endif;
}


function get_the_excerpt_by_id($post_id, $limit=50) {
  global $post;  
  $save_post = $post;
  $post = get_post($post_id);
  $output = get_the_excerpt();
  $post = $save_post;
  return word_limiter($output, $limit, "..." );
}

function icl_post_languages(){
	$langs =  icl_get_languages('skip_missing=0');
	foreach( $langs as $lang ) {
		if( $lang['active'] == 0 ) {
			echo '<a href="'.$lang['url'].'" class="lang-switch" >'.$lang['language_code'].'</a>';
		}
	}
}

function is_page_template_by_id( $template = '', $page_id ) {
if ( ! is_page() )
    return false;
    $page_template = get_page_template_slug( $page_id );
if ( empty( $template ) )
    return (bool) $page_template;
if ( $template == $page_template )
    return true;
if ( 'default' == $template && ! $page_template )
    return true;
    return false;
}




// Mičemo nepotrebne stvari iz headera
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action( 'wp_head', array($sitepress, 'meta_generator_tag' ) );

function strleft($s1, $s2) {
	return substr($s1, 0, strpos($s1, $s2));
}
function current_url(){
    if(!isset($_SERVER['REQUEST_URI'])){
        $serverrequri = $_SERVER['PHP_SELF'];
    }else{
        $serverrequri =    $_SERVER['REQUEST_URI'];
    }
    $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
    $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
    $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
    return $protocol."://".$_SERVER['SERVER_NAME'].$port.$serverrequri;   
}


function read_file($file)
{
	if ( ! file_exists($file))
	{
		return FALSE;
	}

	if (function_exists('file_get_contents'))
	{
		return file_get_contents($file);
	}

	if ( ! $fp = @fopen($file, FOPEN_READ))
	{
		return FALSE;
	}

	flock($fp, LOCK_SH);

	$data = '';
	if (filesize($file) > 0)
	{
		$data =& fread($fp, filesize($file));
	}

	flock($fp, LOCK_UN);
	fclose($fp);

	return $data;
}

function write_file($path, $data, $mode = 'wb')
{
	if ( ! $fp = @fopen($path, $mode))
	{
		return FALSE;
	}

	flock($fp, LOCK_EX);
	fwrite($fp, $data);
	flock($fp, LOCK_UN);
	fclose($fp);

	return TRUE;
}

function delete_files($path, $del_dir = FALSE, $level = 0)
{
	// Trim the trailing slash
	$path = rtrim($path, DIRECTORY_SEPARATOR);

	if ( ! $current_dir = @opendir($path))
	{
		return FALSE;
	}

	while (FALSE !== ($filename = @readdir($current_dir)))
	{
		if ($filename != "." and $filename != "..")
		{
			if (is_dir($path.DIRECTORY_SEPARATOR.$filename))
			{
				// Ignore empty folders
				if (substr($filename, 0, 1) != '.')
				{
					delete_files($path.DIRECTORY_SEPARATOR.$filename, $del_dir, $level + 1);
				}
			}
			else
			{
				unlink($path.DIRECTORY_SEPARATOR.$filename);
			}
		}
	}
	@closedir($current_dir);

	if ($del_dir == TRUE AND $level > 0)
	{
		return @rmdir($path);
	}

	return TRUE;
}

function native_name($lang_code){
	global $wpdb;
	$languages = $wpdb->prefix.'icl_languages';
	$current_lang = ICL_LANGUAGE_CODE;
	$result = $wpdb->get_var( "SELECT english_name FROM $languages WHERE code = '$lang_code'" );
	return $result;
}

function hide_front_end_wp_logo()
{
	if( is_user_logged_in() )
	{
		echo '<style type="text/css"> #wp-admin-bar-wp-logo, #wp-admin-bar-themes, #wp-admin-bar-customize, #wp-admin-bar-comments { display:none; } </style>';
	}
}
add_action( 'wp_head', 'hide_front_end_wp_logo' );


// remove_shortcode('caption');
// remove_shortcode('wp_caption');

function img_caption_shortcode_fix($attr, $content = null) {
	// New-style shortcode with the caption inside the shortcode with the link and image tags.
	if ( ! isset( $attr['caption'] ) ) {
		if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
			$content = $matches[1];
			$attr['caption'] = trim( $matches[2] );
		}
	}

	extract(shortcode_atts(array(
		'id'	=> '',
		'align'	=> 'alignnone',
		'width'	=> '',
		'caption' => ''
	), $attr));

	if ( 1 > (int) $width || empty($caption) )
		return $content;

	if ( $id ) $id = 'id="' . esc_attr($id) . '" ';

	$otp  = '<figure class="' . esc_attr( $align ) . '">';
	$otp .= do_shortcode( $content );
	$otp .= '<figcaption>' . $caption . '</figcaption>';
	$otp .= '</figure>'; 

	return $otp;

	//return '<div ' . $id . 'class="wp-caption ' . esc_attr($align) . '" style="width: ' . (10 + (int) $width) . 'px">'
	//. do_shortcode( $content ) . '<p class="wp-caption-text">' . $caption . '</p></div>';
}
//add_shortcode('caption', 'img_caption_shortcode_fix');

function remove_more_link($link) {
	return "";
}
add_filter('the_content_more_link', 'remove_more_link');

remove_shortcode('gallery');
  function gallery_none() {
  return "";
}

add_shortcode('gallery', 'gallery_none' );
function display_gallery() {
	global $post;
	$save_post = $post;
	$post_name = $post->post_name;
	if (strpos($post->post_content,'[gallery') === false){
	  $has_gallery = false;
  	}else{
	  $has_gallery = true;
  	}

  	preg_match('/\[gallery.*ids=.(.*).\]/', $post->post_content, $ids);
	$images = explode(",", $ids[1]);

	$otp = '';
	$q = new WP_Query( array('post_type' => 'attachment', 'post__in' => $images, 'post_status' => 'any', 'posts_per_page' => -1, 'orderby' => 'post__in', ) );
	if( $q->have_posts() ) : 
		$otp .= '<div class="gallery-content no-flick">';
		$otp .=	'<div class="gallery-inner">';
		$otp .=		'<div class="overlay"></div>';
		$otp .=		'<div class="gallery-sl" data-method="frameSlider">';
		while( $q->have_posts() ) : $q->the_post(); 
			$otp .=	wp_get_attachment_image( get_the_ID(), 'gallery-thumb' );
		endwhile;
		$otp .=		'</div>';
		$otp .=	'</div>';
		$otp .= '</div>';
	endif;
wp_reset_postdata();
$post = $save_post;
return $otp;
}



function the_post_ancestor() {
	global $post;
	if( $post->post_parent == 0 ) {
		$result = $post->ID;
	} else {				
		$ancestors 		= get_post_ancestors( $post->ID ); 
		$result  		= array_pop( $ancestors ); 
	}
	return $result;
}  

function remove_at_sign_from_titles($title){
	$title = str_replace('@'.ICL_LANGUAGE_CODE, '', $title);
	return $title;
}
function remove_at( $string ) {
	$string = str_replace('@'.ICL_LANGUAGE_CODE, '', $string);
	return $string;
}

add_filter( 'the_title', 'remove_at_sign_from_titles',  10, 2 );
add_filter( 'wp_mail_from_name', 'my_mail_from_name' );
function my_mail_from_name( $name )
{
    return '';
}


function add_short_box() {
	add_meta_box( 'postexcerpt', __( 'Short text' ), 'post_excerpt_meta_box', 'press', 'normal', 'core' );
	//add_meta_box( 'postexcerpt', __( 'Text' ), 'post_excerpt_meta_box', 'testimonial', 'normal', 'core' );
}
add_action( 'admin_menu', 'add_short_box' );




function get_page_by_slug($page_slug, $output = OBJECT, $post_type = 'page' ) {
    global $wpdb;
    $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s", $page_slug, $post_type ) );
    if ( $page )
            return get_page($page, $output);
    return null;
}


function get_permalink_by_slug($slug, $post_type = 'page' ) {
	global $wpdb;
	$page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s", $slug, $post_type ) );
	if( $page ) 
		return get_permalink( wpml_id( $page ) );
	return '#';
}

function get_post_by_slug($slug, $post_type = 'page' ) {
	global $wpdb;
	$page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s", $slug, $post_type ) );
	if( $page ) 
		return get_post( wpml_id( $page ) );
	return null;
}

function parse_youtube_url( $url )
{
 $pattern = '#^(?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})(?:.+)?$#x';
 preg_match($pattern, $url, $matches);
 $code = (isset($matches[1])) ? $matches[1] : false;
 return  'https://youtube.googleapis.com/v/'.$code.'?rel=0&autoplay=1';

}

function get_back_arhive_link( $link='' ) {
	$ref = $_SERVER['HTTP_REFERER'];
	if( contains_substr( home_url(), $ref ) && !contains_substr( 'wp-admin', $ref ) ) {
		return $ref;
	} else {
		return $link;
	}
}

function get_gallery_ids($post_id = ""){
	if( $post_id != "" ) {
		$post = get_post( $post_id );
	} else {
		global $post;
	}
  	preg_match('/\[gallery.*ids=.(.*).\]/', $post->post_content, $ids);
  	$images = explode( ",", $ids[1] );
  	return $images;
}

function get_first_image_from_gallery( $id, $size='thumbnail', $attr = '' ) {
	global $post;
	$gallery_arr 	= get_gallery_ids( $id );
 

	$first_img_id 	= array_shift( $gallery_arr );


	$result = wp_get_attachment_image( $first_img_id, $size, false, $attr  );
	return $result;

}


// set order_menu
//add_action( 'save_post', 'set_order_menu' );
function set_order_menu($post_id) {
	$p = get_post( $post_id );
	if( $p->menu_order == 0 && !in_array( $p->post_type , array( 'post', 'page', 'revision' ) ) ) {
		$the_post['ID'] 		= $post_id;
		$the_post['menu_order'] = $post_id;
		wp_update_post( $the_post );
	}
}

//add_action('init', 'wpse42279_add_endpoints');
function wpse42279_add_endpoints()
{
    add_rewrite_endpoint('slovo', EP_PAGES);
    add_rewrite_endpoint('letter', EP_PAGES);
}

function is_ajax_call(){
    return isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
}

add_action( 'wp_head', 'add_no_follow' );
function add_no_follow(){	
	if( contains_substr( 'euroart93.net', current_url() ) ) {
		echo "\n\r" . '<meta name="robots" content="nofollow" />'. "\n\r";
	}
}


function twentyfourteen_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'wp_starter' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'twentyfourteen_wp_title', 10, 2 );




// //ACF hide
// $the_user =  wp_get_current_user();
// if( $the_user->ID != 1 ) {
// 	add_filter('acf/settings/show_admin', '__return_false');
// }


// // ACF Options Page
// if( function_exists('acf_add_options_page') ) {
// 	acf_add_options_page(array(
// 		'page_title' 	=> 'Page Info',
// 		'menu_title'	=> 'Page Info',
// 		'menu_slug' 	=> 'page-general-settings',
// 		'capability'	=> 'edit_posts',
// 		'redirect'		=> false
// 	));
	
// 	// acf_add_options_sub_page(array(
// 	// 	'page_title' 	=> 'Children Menu',
// 	// 	'menu_title'	=> 'Children Menu',
// 	// 	'parent_slug'	=> 'page-general-settings',
// 	// ));
	
// }

function change_month_lang($custom_date) {
	$months_eng = array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    $months_cro = array( 'siječanja', 'veljače', 'ožujka', 'travnja', 'svibnja', 'lipnja', 'srpnja', 'kolovoza', 'rujna', 'listopada', 'studenog', 'prosinca');
    $newDate = str_ireplace($months_eng, $months_cro, $custom_date);
    return $newDate;
}



add_filter ('woocommerce_add_to_cart_redirect', 'redirect_to_checkout');
function redirect_to_checkout() {
global $woocommerce;
$checkout_url = $woocommerce->cart->get_checkout_url();
return $checkout_url;
}

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );


// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     unset($fields['order']['order_comments']);
     unset($fields['billing']['billing_phone']);
     unset($fields['billing']['billing_company']);
     unset($fields['billing']['billing_address_2']);
     unset($fields['billing']['billing_state']);
     unset($fields['billing']['billing_country']);
     
     $fields['billing']['billing_first_name']['placeholder'] = 'Ime';
     $fields['billing']['billing_last_name']['placeholder'] = 'Prezime';
     $fields['billing']['billing_email']['placeholder'] = 'E-mail adresa';


     return $fields;
}

function title_span($string) {
	$title_output = preg_replace("/\*(.*?)\*/", "<span>$1</span>", $string);
    return $title_output;
}



add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment( $fragments ) 
{
    global $woocommerce;
    ob_start(); ?>

    <a href="<?php echo WC()->cart->get_cart_url(); ?>" class="cart"><span><?php echo sprintf (_n( '%d video u košarici', '%d videa u košarici', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?></span></a>

    <?php
    $fragments['a.cart'] = ob_get_clean();
    return $fragments;
}

/* ovo je moglo i bolje :D  ( it's something! )*/
function remove_product_from_cart() {

	if( is_user_logged_in() && WC()->cart->get_cart_contents_count() > 0 ) {
		
		global $wpdb;
		$WC = WC();
		$user_id = get_current_user_id();
	    $customer_email = $current_user->email;
	    $args = array(
	        'post_type' => 'product',
	        'posts_per_page' => -1
	        );
	    $loop = new WP_Query( $args );
	    if ( $loop->have_posts() ) {
	    	$products_array = array();
	    	$current_user = wp_get_current_user();
			$date = date('Y-m-d H:i:s');
			$now = time();
	        while ( $loop->have_posts() ) : $loop->the_post(); 
	        	$_product = get_product( $loop->post->ID );
				
				$downloads_table = $wpdb->prefix . 'woocommerce_downloadable_product_permissions';
				$userResults = $wpdb->get_results( "SELECT * FROM $downloads_table WHERE `user_id` = $current_user->ID AND `product_id` = $_product->id AND `user_email` = '$current_user->user_email' AND `access_expires` >= '$date'");
				if(!empty($userResults)) {
					$expires = $userResults[0]->access_expires;
					if ( wc_customer_bought_product( $current_user->user_email, $current_user->ID, $_product->id) && $expires >= $date ) {
						$products_array[] = $_product->id;
					}
				}
	        endwhile;
	    }
	    wp_reset_postdata();

	    if( !empty($products_array) ) {
	    // Cycle through each product in the cart
		    foreach ( $WC->cart->get_cart() as $cart_item_key => $cart_item ) {
		        // Get the Variation or Product ID
		        $prod_id = $cart_item['product_id'];
		        foreach ($products_array as $prod_to_remove) {
		        	if( $prod_to_remove == $prod_id ) {
						$WC->cart->set_quantity( $cart_item_key, $cart_item['quantity'] - 1, true  );
			        }
		        }

		    }
		} 

    }
}
add_action('wp_login', 'remove_product_from_cart');

// Only display to administrators
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
}
add_action('after_setup_theme', 'remove_admin_bar');


/**
* Vimeo video duration in seconds
*
* @param $video_url
* @return integer|null Duration in seconds or null on error
*/
function vimeoVideoDuration($video_url) {

   $video_id = (int)substr(parse_url($video_url, PHP_URL_PATH), 1);

   $json_url = 'http://vimeo.com/api/v2/video/' . $video_id . '.xml';

   $ch = curl_init($json_url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($ch, CURLOPT_HEADER, 0);
   $data = curl_exec($ch);
   curl_close($ch);
   $data = new SimpleXmlElement($data, LIBXML_NOCDATA);

   if (!isset($data->video->duration)) {
       return null;
   }

   $duration = $data->video->duration;

   return $duration; // in seconds
}


function sanitize_output($buffer) {
    $search = array(
        '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
        '/[^\S ]+\</s',  // strip whitespaces before tags, except space
        '/(\s)+/s'       // shorten multiple whitespace sequences
    );
    $replace = array(
        '>',
        '<',
        '\\1'
    );
    $buffer = preg_replace($search, $replace, $buffer);
    return $buffer;
}

// add async and defer to javascripts
function wcs_defer_javascripts ( $url ) {
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    if ( strpos( $url, 'jquery.min.js' ) ) return $url;
    return "$url' defer='defer";
}
if( !is_admin() ) {
add_filter( 'clean_url', 'wcs_defer_javascripts', 11, 1 );
}


/**
 * Auto Complete all WooCommerce orders.
 */
add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
function custom_woocommerce_auto_complete_order( $order_id ) { 
    if ( ! $order_id ) {
        return;
    }

    $order = wc_get_order( $order_id );
    $order->update_status( 'completed' );
}