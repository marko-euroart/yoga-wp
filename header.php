﻿<?php ob_start("sanitize_output"); ?>
<?php
/**
 * @package WordPress
 * @subpackage wp_starter
 * @since v1.0
 **/
?>
<!DOCTYPE html>
<!--[if IE 7 ]><html lang="hr" class="no-js ie ie7" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 8 ]><html lang="hr" class="no-js ie ie8" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 9 ]><html lang="hr" class="no-js ie ie9" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="hr" class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title><?php wp_title( '|', true, 'right' ); ?></title>   

    <!-- <link id="style" href="<?php echo get_template_directory_uri(); ?>/css/style-min.css" rel="stylesheet" type="text/css"> -->   
    <link id="style" href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/768.css" type="text/css"  media="only screen and (min-width: 768px)">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/1170.css" type="text/css"  media="only screen and (min-width: 1170px)">

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="XZXxJ1xp-9gJGZgsWtmK_r43g_GW8wLVYybID8mxq6E" />
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script type="text/javascript">
       var admin_url = '<?php echo admin_url( "admin-ajax.php" ); ?>';
       var template_url = '<?php echo get_template_directory_uri(); ?>';
    </script>
    <?php /*
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script defer src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
    */ ?>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php if( !(is_user_logged_in()) ): ?>
<div class="login">
    <div class="container">
    
        <a href="#" class="close-login" data-method="closeLogin">Close</a>

        <form method="post" class="login-form">
            <?php do_action( 'woocommerce_login_form_start' ); ?>

            <div class="group">
                <input type="text" class="input-text" name="username" id="username" placeholder="E mail ili Korisničko ime" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
            </div>
            <div class="group">
                <input class="input-text" type="password" name="password" id="password" placeholder="Password" />
            </div>
            
            <?php do_action( 'woocommerce_login_form' ); ?>

            <div class="group checkbox">
                <input type="checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" data-method="checkBox">
                <label for="rememberme">Zapamti me</label>
            </div>
            <div class="group">
            <?php wp_nonce_field( 'woocommerce-login' ); ?>
                <button type="submit" class="btn-1" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_attr_e( 'Login', 'woocommerce' ); ?></button>
            </div>
            <p class="lost-password">
                <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="lost-password"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
            </p>

            <?php do_action( 'woocommerce_login_form_end' ); ?>
        </form>


        <h3 class="title-2"><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">Još niste naš član? <span>Registrirajte se</span></a></h3>
        <ul class="socials">
            <li class="facebook"><a href="#">Facebook</a></li>
            <li class="youtube"><a href="#">Youtube</a></li>
        </ul>
    </div>
</div>
<?php endif; ?>

<div id="menu-trigger" data-method="navTrigger">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
</div>

<div class="body-wrapper">
<!-- Header -->
<header id="page-header" role="banner">
    <div class="container">

        <div class="site-branding">
            <p class="site-tagline"><?php echo bloginfo('description'); ?></p>
            <?php if( is_front_page() ) : ?>
                <h1 class="site-logo"><a href="<?php echo home_url(); ?>"  title="Site title"><?php echo bloginfo('name'); ?></a></h1>  
            <?php else: ?>
                <div class="site-logo"><a href="<?php echo home_url(); ?>"  title="Site title"><?php echo bloginfo('name'); ?></a></div>  
            <?php endif; ?>
        </div>

        <nav class="main-nav">
            <?php if( !(is_user_logged_in()) ): ?>
            <a href="#" class="login-link" data-method="openLogin">Prijava i registracija</a>
            <?php else: ?>
            <?php 
                global $current_user;
                get_currentuserinfo();
            ?>
            <div class="logged-user">
                <?php if( !empty($current_user->user_firstname) ): ?>
                <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="user"><span><?php echo esc_html($current_user->user_firstname); ?> <?php echo esc_html($current_user->user_lastname); ?></span></a>
                <?php else: ?>
                <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="user"><span><?php echo esc_html($current_user->user_nicename); ?></span></a>
                <?php endif; ?>
                <div class="cart-main">
                    <a href="<?php echo WC()->cart->get_cart_url(); ?>" class="cart"><span><?php echo sprintf (_n( '%d video u košarici', '%d videa u košarici', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?></span></a>
                </div>
                <a href="<?php echo home_url(); ?>/?customer-logout=true">Odjava</a>
            </div>
            <?php endif; ?>

            <?php wp_nav_menu( menu_args( 'primary_menu' ) ); ?>
               
        </nav>
        
    </div>
</header>
<!-- /Header -->
