<?php 
//Template name: Home
get_header(); ?>
<!-- Promo -->
<div id="promo" class="home-promo">
	<div class="container">
		
		<h2 class="title-3">
			<?php the_title(); ?>
			<?php if( get_field('subtitle') ) {
				echo '<span>'. get_field('subtitle') .'</span>';
			} ?>
		</h2>

		<a href="https://vimeo.com/153888118" class="link-2 fancybox-media">Pogledajte besplatni video</a>

	</div>
</div>
<!-- /Promo -->

<!-- Main -->
<div id="main">

	
	<section id="instructors" data-method="instAnchors">
		<div class="container">
			<h2 class="title-1">
				Instruktori
				<?php /* <span>One će vam vratiti zdravlje, liniju i ravnotežu</span> */ ?>
			</h2>

			<?php $q = new WP_Query( array( 'posts_per_page' => -1, 'post_type' => 'instructor', 'order' => 'ASC', 'orderby' => 'menu_order' ) ); ?>
			<?php if( $q->have_posts() ) : ?>
			<div class="instructors-wrapper">
				<?php $c=1; while( $q->have_posts() ) : $q->the_post(); ?>
				<?php 
					$cert_name = get_field('cert_name');
				?>
				<div class="instructor">
					<a href="<?php echo get_permalink( wpml_id(9) ); ?>?section=inst<?php echo get_the_id(); ?>">
						
						<?php if( has_post_thumbnail() ) : ?>
						<div class="instructor-thumb">
							<?php the_post_thumbnail( 'instructor-thumb'); ?>
						</div>
						<?php endif; ?>

						<div class="instructor-name">
							<?php 
								$ins_name = get_the_title(); 
								$pieces = explode(' ', $ins_name);
								echo '<p>'. $pieces[0] .' <span>'. $pieces[1] .'</span></p>';
							
								if($cert_name) {
									echo '<p class="certificate">'. $cert_name .'</p>';
								}
							?>
						</div>
					</a>
				</div>
				<?php $c++; endwhile; ?>
			</div>
			<?php endif; wp_reset_postdata(); ?>

			<?php /*
			<blockquote><span>“</span>Mauris quis dignissim neque. Pellentesque vel imperdiet nisl. Nam eu 
			consectetur tortor, nec ullamcorper nisi.</blockquote>
			<div class="instructor-info">
				<article class="user-content">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor 
					incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
					exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				</article>
				<div class="certificates">
					<p>Certifikati</p>
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/high-def/certificate-1.png" media="(max-width: 768px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/assets/certificate-1.png" alt="RYT 200">
					</picture>
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/high-def/certificate-2.png" media="(max-width: 768px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/assets/certificate-2.png" alt="RYT 500">
					</picture>
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/high-def/certificate-3.png" media="(max-width: 768px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/assets/certificate-3.png" alt="E-RYT 500">
					</picture>	
				</div>
			</div>
			*/ ?>
		</div>
	</section>

	<section id="video-shop">
		<div class="container">
			<h1 class="title-1">
				Video Shop
				<span>sadržaj iz ponude</span>
			</h1>


			<?php 
			$args = array(
			    'posts_per_page' => -1,
			    // 'tax_query' => array(
			    //     //'relation' => 'AND',
			    //     array(
				   //     'taxonomy' => 'product_type',
				   //    'field' => 'slug',
				   //    'terms' => 'simple'
			    //     )
			    // ),
			    'post_type' => 'product',
			    'orderby' => 'title',
			);
			$the_query = new WP_Query( $args );
		?>

		<?php if ( $the_query->have_posts() ) : ?>

			<?php woocommerce_product_loop_start(); ?>

			<?php woocommerce_product_subcategories(); ?>

			<div class="video-blocks">
				<h3 class="title-4">Paketi</h3>
				<div class="video-blocks-wrapper">

					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php 
						$chained_products_id = $post->ID;
						$total_chained_details  = $wc_chained_products->get_all_chained_product_details( $chained_products_id );
						if( empty($total_chained_details) ) continue; 
						?>
						<?php wc_get_template_part( 'content', 'product' ); ?>

					<?php endwhile; // end of the loop. ?>

					
				</div>
			</div>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif; wp_reset_query(); ?>

		<?php 
			$args = array(
			    'posts_per_page' => -1,
			    'meta_query' => array(
			        array(
						'key'	  	=> 'show_on_front',
						'value'	  	=> '1',
						'compare' 	=> '=',
					)
			    ),
			    'post_type' => 'product',
			    'orderby' => 'title',
			);
			$the_query = new WP_Query( $args );
		?>

		<?php if ( $the_query->have_posts() ) : ?>

			<?php woocommerce_product_loop_start(); ?>

			<?php woocommerce_product_subcategories(); ?>

			<div class="video-blocks">
				<h3 class="title-4">Pojedinačni</h3>
				<div class="video-blocks-wrapper">

					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php 
						$chained_products_id = $post->ID;
						$total_chained_details  = $wc_chained_products->get_all_chained_product_details( $chained_products_id );
						if( !empty($total_chained_details) ) continue; 
						?>
						<?php wc_get_template_part( 'content', 'product' ); ?>

					<?php endwhile; // end of the loop. ?>

					
				</div>
			</div>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif; wp_reset_query(); ?>


		<?php get_template_part('partials/video-section'); ?>

		</div>
	</section>

	<section id="joga-info">
		<div class="container">
			<h1 class="title-1">
			<?php $benefit_id = 11; ?>
			<?php echo get_the_title( $benefit_id ); ?>
			<?php if( get_field('subtitle', $benefit_id) ) {
				echo '<span>'. get_field('subtitle', $benefit_id) .'</span>';
			} ?>
			</h1>
			<?php 
				$left_content = get_field('left_content', $benefit_id);
				$right_content = get_field('right_content', $benefit_id);
			?>
			<div class="left-side">
				<?php if($left_content): ?>
				<h3 class="highlighted"><?php echo $left_content; ?></h3>
				<?php endif; ?>
				<?php if($right_content): ?>
				<article class="user-content">
					<p><?php echo $right_content; ?></p>
					<a href="<?php echo get_permalink($benefit_id); ?>" class="btn-2">Više o prednostima</a>
				</article>
				<?php endif; ?>
			</div>

			<?php if( have_rows('benefit_list', $benefit_id) ): ?>

			<div class="right-side user-content">
				<h3 class="highlighted">Dobrobiti</h3>
				<ol class="highlighted-ol">
					<?php 
				   	while ( have_rows('benefit_list', $benefit_id) ) : the_row();
						echo '<li><p><strong>'. get_sub_field('title') .'</strong></p></li>';
				    endwhile; 
				    ?>
					
				</ol>
			</div>
			<?php endif; ?>
		</div>
	</section>
</div>
<!-- /Main -->
<?php get_footer(); ?>