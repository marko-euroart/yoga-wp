<?php 
//Template name: School
get_header(); ?>


<?php 
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header-image' );
	$style = '';
	if( !empty($thumb) ) {
		$url = $thumb['0']; 
		$style = 'style="background: url('. $url .') no-repeat center bottom"';
	}
?>
<div id="promo" <?php echo $style; ?>>
	<div class="container">
		<h1 class="title-3">
			<?php the_title(); ?>
			<?php if( get_field('subtitle') ) {
				echo '<span>'. get_field('subtitle') .'</span>';
			} ?>
		</h1>
	</div>
</div>



<div id="main">
	<section id="program-description">
		<div class="container">
			<div class="side-content -left-sided">
				<div class="img-wrapper-certificate">
					<img src="<?php echo get_template_directory_uri(); ?>/img/assets/certificate-1-big.png" alt="Certificate">
					<div class="img-description">
						<p>Registered <br> Yoga <br> Teacher</p>
					</div>
				</div>
			</div>
			<div class="main-content -right-sided">
				<div class="user-content -program-description">
					<h3 class="highlighted -uppercase">Program: 200 RYT osnovna obuka</h3>
					<p>*Jednogodišnje školovanje VIKEND OBUKA<br>
					*INTEZIV program, 200 sati</p>
				</div>
			</div>
		</div>
	</section>
	<section id="lr-content-wrapper">
		<div class="container">
			<div class="side-content -left-sided">
				<ul class="side-menu">
					<li><a href="#"><span>Teacher training</span></a></li>
					<li><a href="#"><span>RYS 200</span></a></li>
					<li><a href="#"><span>Nastavni plan</span></a></li>
					<li><a href="#"><span>Edukacijski tim</span></a></li>
					<li><a href="#"><span>Školarina</span></a></li>
					<li><a href="#"><span>Uvjeti</span></a></li>
					<li><a href="#"><span>Završni ispit</span></a></li>
					<li><a href="#"><span>Kalendar i prijava</span></a></li>
				</ul>
			</div>
			<div class="main-content -right-sided">
				<div class="content-block">
					<div class="user-content">
						<h3 class="highlighted-title">Kalendar TT programa</h3>
						<p>Nova školska godina počinje …………………………..., a upisni rok je do ……………….ili do popunjenja slobodnih mjesta.</p>
						<p>O prihvatu u program obuke studenti će biti obaviješteni u roku od dva tjedna nakon primitka upisnog materijala.</p>
					</div>
				</div>
				<div class="content-block">
					<div class="user-content">
						<h4 class="highlighted-title-small">Uvjeti plaćanja</h4>
						<p>Školarina se može uplatiti (označiti izabrani način plaćanja):</p>
						<ul class="plain-list">
							<li>U cijelosti pri upisu</li>
							<li>U tri rate: listopad / ožujak / rujan</li>
							<li>Kroz 12 mjesečnih rata</li>
						</ul>
					</div>
				</div>
				<div class="content-block">
					<div class="user-content">
						<h4 class="highlighted-title-small">Školarina obuhvaća</h4>
						<ul class="plain-list">
							<li>Teaching fee.</li>
							<li>Poseban popust na mjesečne TT pakete za vrijeme trajanja obuke.</li>
							<li>Padma Yoga ilustrirani TT udžbenik na hrvatskom jeziku.</li>
							<li>Dio obavezne literature (e-format).</li>
							<li>Pismeni i praktični ispiti.</li>
							<li>„2 x kartica za polaznike TT" neograničen broj termina.</li>
							<li>Besplatan pristup online satovima  www.yogatecajevi.com (2 mjeseca).</li>
							<li>Besplatno pohađanje Mysore satova u toku prva 3 mjeseca obuke.</li>
							<li>Posebna cijena naših mjesečnih paketa + NEOGRANIČENI TT paket za samo 150 kn (popusti kreću od trenutka prijave na TT).</li>
						</ul>
					</div>
				</div>
				<div class="content-block" data-method="checkBox">
					<h3 class="highlighted-title -uppercase -mb0">Online prijava 
					<span>Obrazac za upis u <strong>PADMA YOGA TT 200 ŠKOLU</strong></span></h3>
					<?php echo do_shortcode( '[gravityform id=1 title=false description=false]' ); ?>
				</div>
			</div>
		</div>
	</section>
</div>


<?php get_footer(); ?>